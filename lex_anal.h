#ifndef __LEX_ANAL__
#define __LEX_ANAL__

#include <stdio.h>

#define true 1
#define false 0

#ifndef __NumberKeyWords__
#define __NumberKeyWords__
const int NumberOfKeyWords = 18;

const char *KeyWords[]={
    "else\0", "function\0", "if\0", "return\0", "while\0", "true\0", "false\0", "null\0", "boolval\0", "doubleval\0",
    "intval\0", "strval\0", "get_string\0", "put_string\0", "strlen\0", "get_substring\0", "find_string\0", "sort_string\0"
};
#endif
typedef enum {
    StateStart,        // 00 - Záklaní stav
    StateID,           // 01 - Identifikátor
    StateInt,          // 02 - Integer
    StateDouble,       // 03 - Double
    StatePlus,         // 04 - +
    StateMinus,        // 05 - -
    StateLP,           // 06 - (
    StateRP,           // 07 - )
    StateMore,         // 08 - >
    StateLess,         // 09 - <
    StateLessEQ,       // 10 - <=
    StateMoreEQ,       // 11 - >=
    StateComma,        // 12 - Čárka
    StateMul,          // 13 - *
    StateDiv,          // 14 - /
    StateDol,          // 15 - $
    StateEQ,           // 16 - ===
    StateNEQ,          // 17 - !==
    StateEnd,          // 18 - Koncovy stav
    StateOr,           // 19 - ||
    StateAnd,          // 20 - &&
    StateSemiColon,    // 21 - ;
    StateLBracket,     // 22 - [
    StateRBracket,     // 23 - ]
    StateVar,   	   // 24 - Variable
    StateKeyEnd,       // 25 - Koncoý stav pro klíčová slova - string se neukládá do tokenu
	StateERR,		   // 26 - Chybový stav
	StateIsExp,		   // 27 - Kontrola znaménka exponentu
	StateExp,          // 28 - Číslo s exponentem
	StateLCom,         // 29 - Řádkový komentář
	StateBCom,         // 30 - Blokový komentář
	StateIsDouble,     // 31 - Kontrola znaku za desetinnou čárkou
	StateComEnd,       // 32 - Ukonceni blokoveho komentare
	StateQM,           // 33 - Dvojite uvozovky
	StateStr,          // 34 - Čtení řetězce
	StateIsExp2,       // 35 - Kontrola číslice po +- v exponentu
	StateLBrace,       // 36 - Levá složená závorka
	StateRBrace,       // 37 - Pravá složená závorka
	StateIsEQ,         // 38 - Kontrola počtu '='
	StateAssign,       // 39 - Přiřazení
	StateIsNEQ,        // 40 - Mezistav nerovnosti
	StateDot,          // 41 - Tečka
	StatePHP,		   // 42 - <?
	StatePHP1,         // 43 - <?p
	StatePHP2,         // 44 - <?ph    
	StateElse,         // 45 - Else			+	Nemenit poradi klicovych slov!!!
	StateFunction,     // 46 - Function		+
	StateIf,           // 47 - If			+
	StateReturn,       // 48 - Return		+
	StateWhile,        // 49 - While		+
	StateTrue,         // 50 - True			+
	StateFalse,        // 51 - False		+
	StateNull,         // 52 - Null			+ 
    StateBoolVal,      // 53 - Vstavana funkcia
    StateDoubleVal,    // 54 - -||-
    StateIntVal,       // 55 - -||-
    StateStrVal,       // 56 - -||-
    StateGetStr,       // 57 - -||-
    StatePutStr,       // 58 - -||-
    StateStrLen,       // 59 - -||-
    StateGetSBStr,     // 60 - -||-
    StateFindStr,      // 61 - -||-
    StateSortStr,      // 62 - -||-
// Nasledujici stavy se pouzivaji pri syntakticke analyze pro urceni derivacniho pravidla
	Program,		   // - 63
	Body,			   // - 64
    FList,			   // - 65
    Param,			   // - 66
    Params,			   // - 67
    RParam,			   // - 68
    List,			   // - 69
    Else,			   // - 70
    Expr,			   // - 71
    Inc,			   // - 72
    String,			   // - 73
    RString,  		   // - 74  
    PrecExp, 		   // - 75
    Const,			   // - 76
    Operand,           // - 77
    Print,			   // - 78
    E                 // - 79
}Sstate;


typedef struct {
    Sstate state;
    char *data;
}Token;

Token getToken(FILE *source,int* line);

#endif
