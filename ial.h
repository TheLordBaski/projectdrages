#ifndef __IAL__
#define __IAL__
#include "lex_anal.h"
#include "tstring.h"

#define ALPHABET 256


#include <stdbool.h>

// Bude se ukladat do varType do struktury tData
enum EnumTypes
{
  TYPE_INT = 0,
  TYPE_DOUBLE,
  TYPE_STRING,
  TYPE_NULL, // PODLE ZADANI
  TYPE_VAR,
  TYPE_BOOL,
  TYPE_FUNCTION
};

typedef union 
{
  int    int_var;
  double double_var;
  string string_var;
  bool   bool_var;
} tValues;


typedef struct
{
  int     varType;  // typ dane promenne, v nasem pripade vzdy int
  tValues varValue; // pro ruzne typy nutnost resit pomoci unie
  bool    isTemp;
  bool    isVar;
  void *nextArg;
} tData;
  
typedef struct treeItem
{
  char *key;                  // klic, podle ktereho se bude vyhledavat = nazev identifikatoru
  tData data;                  // data, ktera jsou ke klici pridruzena
  struct treeItem *LTreeItem;  // ukazatel ne levy prvek 
  struct treeItem *RTreeItem;  // ukazatel na pravy prvek
  struct treeItem *PTreeItem;  // ukazatel na otcovsky prvek
} tTreeItem, *tTreeItemPtr;

typedef struct
{
  tTreeItem *root;
} tSymbolTree;

void treeInit(tSymbolTree *T);
tTreeItemPtr treeInsert(tSymbolTree *T, char *key, int varType,char *data);
tTreeItemPtr nodeInsert(tTreeItemPtr *T, char *key, int varType,char *data);
tTreeItem *treeSearch(tSymbolTree *T, char *key);
tTreeItem *nodeSearch(tTreeItem *node, char *key);
void dataToStruct(tTreeItemPtr *T, int varType, char *data);
void treeFree(tSymbolTree *T);
void nodeFree(tTreeItem *node);
int treeCopy(tSymbolTree *Orig, tSymbolTree *Copy);
int nodeCopy(tTreeItemPtr *Orig, tTreeItemPtr *Copy);
int copyData(tTreeItemPtr *Orig, tTreeItemPtr *Copy);
void printTree(tSymbolTree *T);
void printNode(tTreeItem *node);



typedef struct{
  int arr[ALPHABET];
} TJumpCharArray;

typedef struct{
  int *arr;
} TJumpMatchArray;



void computeMatchJump(string *str, TJumpMatchArray *skipTableMatch);
void computeCharJumps(string *str, TJumpCharArray *skipTableChar);
int min(int a, int b);
int max(int a, int b);
int findString(string *str ,string *match, int *result);
void swap (char*, int, int);
void buildHeap (char*, int);
void maxHeapify (char*, int, int);
void heapSort (char*, int);
void floydHeapify (char*, int);

#endif
