#ifndef __TERROR__
#define __TERROR__

#include "lex_anal.h"

typedef enum{
	LEXICAL_ERR = 1,
	SYNTAKTIC_ERR = 2,
	FUNCTION_ERR = 3,
	ARGUMENT_ERR = 4,
	UNDEC_VAR_ERR = 5,
	DIV_ZERO_ERR = 10,
	DOUBLEVAL_ERR = 11,
	COMPAT_ERR = 12,
	SEMANTIC_ERR = 13,
	INTERNAL_ERR = 99,
	E_OK = 0,
  	E_MALLOC = 99
}err;


const char *UStates[]={
    [StateID] = "name of the function",           // 01 - Identifikátor
    [StateDouble] = "Double",       // 03 - Double
    [StatePlus] = "'+'",         // 04 - +
    [StateMinus] = "'-'",        // 05 - -
    [StateLP] = "'('",           // 06 - (
    [StateRP] = "')'",           // 07 - )
    [StateMore] = "'>'",         // 08 - >
    [StateLess] = "'<'",         // 09 - <
    [StateLessEQ] = "'<='",       // 10 - <=
    [StateMoreEQ] = "''>='",       // 11 - >=
    [StateComma] = "','",          // 12 - Čárka
    [StateMul] = "'*'",          // 13 - *
    [StateDiv] = "'/'",          // 14 - /
    [StateDol] = "'$'",          // 15 - $
    [StateEQ] = "'==='",           // 16 - ===
    [StateNEQ] = "'!=='",          // 17 - !==
    [StateSemiColon] = "';'",    // 21 - ;
    [StateVar] = "variable",      // 24 - Neukonceny string
	[StateExp] ="exponent",          // 28 - Číslo s exponentem
	[StateQM] ="\"",           // 33 - Dvojite uvozovky
	[StateStr] ="char*",          // 34 - Čtení řetězce
	[StateLBrace] ="'{'",       // 36 - Levá složená závorka
	[StateRBrace] ="''}'",       // 37 - Pravá složená závorka
	[StateAssign] ="'='",       // 39 - Přiřazení
	[StateDot] ="'.'",        // 41 - Tečka
	[StatePHP] ="<?php",		   // 42 - <?php
	[StateReturn] ="return",       // 48 - Return
	[PrecExp] = "expression",
	[StateWhile] = "while",
	[StateIf] = "if",
	[StateFunction] = "function",
	[Expr] = "expression",
	[StateEnd] = "EOF",
	[List] = "block of code",
	[String] = "string",
	[Else] = "else"
};


err tError = E_OK;

void AllocERR(void);
void UnexpErr(Sstate Estate, Sstate Ustate,int line);
void setErr(err Error);
err ErrRet();
int getNum();

#endif
