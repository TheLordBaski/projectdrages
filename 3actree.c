
#include "3actree.h"
#include "tstack.h"
#include <string.h>

// globalni promenna na posbirani ass_node -> jelikoz je slozite je uvolnit
TSStack node_collector;

void ass_nodeInsert(ASSItemPtr *T, int operation,tTreeItemPtr operand)
{
    // vlozime do tabulky novou polozku
    if ((*T = malloc(sizeof(struct ASSItem))) == NULL)
    {
      AllocERR();
      setErr(INTERNAL_ERR);
      return;
    }
    
    (*T)->operation = operation;
    (*T)->operand = operand;
    
    //ulozim si ukazatel
    TSSPush(&node_collector, *T);
}

void assNodesInit()
{
  TSSInit(&node_collector);
}

void assNodesFree()
{
  ASSItemPtr ptrTmp;
  while(!TSSEmpty(&node_collector))
  {
    ptrTmp = (ASSItemPtr)TSSTop(&node_collector);
    TSSPop(&node_collector);
    free(ptrTmp);
  }
  freeSTstack(&node_collector);
}

tTreeItemPtr createTmpVar(int rank, tSymbolTree *T)
{
  char text[12];
  sprintf(text, "@var%d", rank);

  return treeInsert(T, text,TYPE_VAR, "");

}

void listInit(tListInstr *L)
// funkce inicializuje seznam instrukci
{
  L->first  = NULL;
  L->last   = NULL;
  L->active = NULL;
}
  
void listFree(tListInstr *L)
// funkce dealokuje seznam instrukci
{
  tLItem ptr;
  TSStack *stack;
  
  while (L->first != NULL)
  {
    ptr = L->first;
    L->first = L->first->nextptr;
    // uvolnime celou polozku
    if(ptr->ins != NULL)
    {
      if(ptr->ins->operation == TO_LABEL && ptr->ins->label != NULL)//uvolnime label
      { 
        free(ptr->ins->label);
        if(ptr->ins->jump != NULL)
        {
          treeFree(ptr->ins->jump);
          free(ptr->ins->jump);
        }
      }
      if(ptr->ins->operation == TO_FCE) //uvolnime "jump"
      {
        if(ptr->ins->label != NULL)
        {
          free(ptr->ins->label->name);
          free(ptr->ins->label);
        }
        if(ptr->ins->jump != NULL)
        {
          stack = ptr->ins->jump;
          freeSTstack(stack);
          free(ptr->ins->jump);
        }
      }
    }
    free(ptr->ins);
    free(ptr);
  }
}

void listInsertLast(tListInstr *L, TA3ac* ins)
// vlozi novou instruci na konec seznamu
{
  tLItem newItem;
  newItem = malloc(sizeof(struct tListItem));
  newItem->ins = ins;
  newItem->nextptr = NULL;

  if (L->first == NULL)
     L->first = newItem;
  else
     L->last->nextptr=newItem;
  L->last=newItem;
}

void listFirst(tListInstr *L)
// zaktivuje prvni instrukci
{
  L->active = L->first;
}

void listNext(tListInstr *L)
// aktivni instrukci se stane nasledujici instrukce
{
  if(L->active != NULL)
    L->active = L->active->nextptr;
}

void listGoto(tListInstr *L, tLItem gotoInstr)
// nastavime aktivni instrukci podle zadaneho ukazatele
{
  L->active = (tLItem) gotoInstr;
}

void *listGetPointerLast(tListInstr *L)
// vrati ukazatel na posledni instrukci
{
  return (void*) L->last;
}

tLItem  listGetData(tListInstr *L)
// vrati aktivni instrukci
{
  if (L->active == NULL)
  {
    //error
    return NULL;
  }
  else 
    return L->active;
}


// DOCASNE KVULI DEBUGOVANI
void printList(tListInstr *L)
{
  tLItem ptr = L->first;
  while (ptr != NULL)
  {

    printf("\n----------- INSTRUKCE -----------\n");
    printf("|-- operace: '%s'\n\n",  operators[ptr->ins->operation]);
    if(ptr->ins->operation == TO_JMP || ptr->ins->operation == TO_FCE)
    {
      printf("OPERAND1: "); printListItem(ptr->ins->op1);
      printf("OPERAND2: "); printListItem(ptr->ins->op2);
      printf("RESULT:   "); printListItem(ptr->ins->result);
      printf("JUMP: %s\n\n", ptr->ins->jump == NULL ? "NEEXISTUJE" : "EXISTUJE");
      printf("NAME: %s\n\n", ptr->ins->label == NULL ? "NEEXISTUJE" : ptr->ins->label->name);
    }
    else{
      if(ptr->ins->operation == TO_LABEL){
        printf("NAME: %s\n\n", ptr->ins->label == NULL ? "NEEXISTUJE" : ptr->ins->label->name);
        printf("OPERAND1: "); printListItem(ptr->ins->op1);
        printf("OPERAND2: "); printListItem(ptr->ins->op2);
      }
      else{
        printf("OPERAND1: "); printListItem(ptr->ins->op1);
        printf("OPERAND2: "); printListItem(ptr->ins->op2);
        printf("RESULT:   "); printListItem(ptr->ins->result);
      }  
    }
    
    printf("-------- KONEC INSTRUKCE --------\n");

    ptr = ptr->nextptr;
  }
}

void printListItem(tTreeItemPtr ptr)
{
  if(ptr == NULL)
  {
    printf("|--typ: NOTEXIST\n\n");
    return;
  }

  switch(ptr->data.varType)
  {
    case TYPE_INT:
      printf("|--typ: int\thodnota %d\n\n", ptr->data.varValue.int_var);
      break;
    case TYPE_DOUBLE:
      printf("|--typ: dbl\thodnota: %f\n\n", ptr->data.varValue.double_var);
      break;
    case TYPE_STRING:
      printf("|--typ: str\thodnota: %s\tdelka: %d\n\n",  ptr->data.varValue.string_var.data, ptr->data.varValue.string_var.length);
      break;
    case TYPE_VAR:
      printf("|--typ: var\tkey: %s\n\n", ptr->key);
      break;
    case TYPE_BOOL:
      printf("|--typ: bool\thodnota:%s\n\n", (ptr->data.varValue.bool_var == true) ? "true" : "false");
      break;
    case TYPE_NULL:
      printf("|--typ: null\n\n");
  
  }

}

