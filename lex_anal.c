#include <stdio.h>
#include "tstring.h"
#include "lex_anal.h"
#include <ctype.h>
#include "terror.h"
#include <string.h>

// '!!' = je potreba neco dodelat :D


Token token;
int got_white=0;
int str_err=0;

/*
 * Inicializácia Tokenu
 */


static void initToken(void){
    token.data=NULL;
    token.state=StateStart;
}

/*
 * Funkcia prejde klúčové slová a zistí či sa tam nachádza 
 * @parameter 1 = slovo, ktoré kontrolujeme
 * @return StateKeyWord - ak je to klúčové slovo
 * 		   StateIdent - identifikátor
 */

static Sstate isKeyWord(string *slovo){
	for(int i = 0; i < NumberOfKeyWords; i++){
		if(strcmp(slovo->data,KeyWords[i]) == 0)
			return StateElse+i;
	}
	return StateID;
}


Token getToken(FILE *source,int *line){

	initToken();

	int fin = false;
	Sstate state = StateStart;
	string str;
	stringCreate(&str);
	char c;

	while ( !fin && (c= fgetc(source)) ){
		switch (state){

			case StateStart: {
					if ( (isalpha(c)) || c == '_'){
						stringAdd(&str ,c);
						state=StateID;
					}
					else if (isdigit(c)){
						stringAdd(&str ,c);
						state = StateInt;
					}
					else if (isspace(c)){
						got_white = 1;
						if(c=='\n')
							{
								++*line;
							}
					}
					else {
						switch (c){
							case '=': {
								state=StateIsEQ;
								break;
							}
							case '-': {
								state=StateEnd;
								token.state=StateMinus;
								break;
							}
							case '+': {
								state=StateEnd;
								token.state=StatePlus;
								break;
							}
							case '*': {
								state=StateEnd;
								token.state=StateMul;
								break;
							}
							case '/': {
								got_white=1;
								state=StateDiv;
								break;
							}
							case '$': {
							state=StateDol;
							break;
							}
							case '(': {
							state=StateEnd;
							token.state=StateLP;
							break;
							}
							case ')': {
							state=StateEnd;
							token.state=StateRP;
							break;
							}
							case '"': {	
							state=StateStr;	
							break;
							}
							case '{':{
							state=StateEnd;	
							token.state=StateLBrace;	
							break;
							}
							case '}':{
							state=StateEnd;	
							token.state=StateRBrace;
							break;
							}
							case '!':{
							state=StateIsNEQ;	
							break;
							}
							case ';':{
							token.state=StateSemiColon;
							state=StateEnd;	
							break;
							}
							case EOF:{
							token.state=StateEnd;	
							ungetc(c,source);
							state=StateEnd;
							break;
							}
							case '>':{
							state=StateMore;	
							break;	
							}
							case '<':{
							state=StateLess;
							break;
							}
							case ',':{
							token.state=StateComma;
							state=StateEnd;
							break;
							}
							case '.':{
							token.state=StateDot;
							state=StateEnd;
							break;
							}
							
							default :{
							fprintf(stderr,"Line %d: Unexpected character in source file!\n",*line);
							setErr(LEXICAL_ERR);
							state=StateStart;
							break;
							}
							} // konec switch
						}	// konec else
				break; //StateStart
				}

			case StateVar:{

				if ( (isalpha(c)) || (isdigit(c)) || c=='_'){
					stringAdd(&str ,c);
				}
				else {
					ungetc(c,source);
					token.state=StateVar;
					state = StateEnd;
					token.data=str.data;
				}
				break; // StateVar
			}


			case StateID:{

				if ( (isalpha(c)) || (isdigit(c)) || c=='_'){
					stringAdd(&str ,c);
				}
				else {
					ungetc(c,source);
					token.state=isKeyWord(&str);
					if (token.state == StateID){
						state = StateEnd;
					}
					else {
						state = StateKeyEnd;
					}	
				}
				break; //ID
			}
			case StateDol:{
				if ( (isalpha(c)) ||  c=='_'){
					stringAdd(&str ,c);
					state= StateVar;
				}
				else {
					ungetc(c,source);
					token.state=StateERR;
					state = StateERR;
					fprintf(stderr,"Line %d: Expecting identifier behind '$' character.\n",*line);
					setErr(LEXICAL_ERR);
				}
				break; //StateDol
			}

			case StateInt:{
				if ( (isdigit(c))){
					stringAdd(&str ,c);
				}
				else if ( c== '.'){
					stringAdd(&str ,c);
					state=StateIsDouble;
				}
				else if ( c=='e' || c=='E'){
					stringAdd(&str, c);
					state=StateIsExp;
				}
				else {
					ungetc(c,source);
					token.state=StateInt;
					//prevod cisla + ulozeni do tokenu !!
					state = StateEnd;
				}
			break; //StateInt
			}
			
			case StateIsDouble:{
				if (isdigit(c)){
					stringAdd(&str, c);
					state = StateDouble;
				}
				else {
					ungetc(c,source);
					token.state=StateDouble;
					state = StateERR;
					fprintf(stderr,"Line %d: Expecting number behind '.'\n",*line);
					setErr(LEXICAL_ERR);
				}
				
				break; //StateIsDouble;
			} 
			
			case StateDouble:{
					if (isdigit(c)){
						stringAdd(&str, c);
					}
					else if(c=='e' || c=='E') {
					  stringAdd(&str, c);
					  state = StateIsExp;
					}
					else {
						ungetc(c,source);
						token.state=StateDouble;
						state = StateEnd;	
					}
				break; // StateDouble
			}
			
			case StateIsExp:{
				if(c=='+' || c=='-'){
					stringAdd(&str, c);
					state=StateIsExp2;
				}
				else if(isdigit(c)){
					stringAdd(&str, c);
					state=StateExp;
				}
				else {
					ungetc(c, source);
					state=StateERR;
					token.state=StateExp;
					fprintf(stderr,"Line %d: Exponent has no digits.\n",*line);
					setErr(LEXICAL_ERR);
				}
				break; //StateIsExp	
			}
			
			case StateIsExp2:{
				if(isdigit(c)){
					stringAdd(&str, c);
					state=StateExp;
				}
				else {
					ungetc(c, source);
					state=StateERR;
					token.state=StateExp;
					fprintf(stderr,"Line %d: Exponent has no digits.\n",*line);
					setErr(LEXICAL_ERR);
				}
				break; // StateIsExp2
			}
			
			case StateExp:{
				if(isdigit(c)){
					stringAdd(&str, c);
				}
				else { 
					ungetc(c,source);
					token.state=StateExp;
					state = StateEnd;
				}
			break; //StateExp
			}
			
			case StateDiv:{
				if (c=='/'){
					state=StateLCom;
				}
				else if (c=='*'){
					state=StateBCom;
				}
				else {
					ungetc(c, source);
					token.state = StateDiv;
					state = StateEnd;
				}
			break; // StateDiv
			}
			
			case StateLCom:{
				if (c=='\n'){
					++*line;	
					state=StateStart;
				}
			break; // StateLCom	
			}
			
			case StateBCom:{
				if (c=='\n'){
						++*line;
				}
				else if (c=='*'){
					state=StateComEnd;
				}
				else if (c==EOF){
					fprintf(stderr,"Line %d: Missing block comment terminating character.\n",*line-1);
					setErr(LEXICAL_ERR);
					token.state=StateEnd;
					state=StateEnd;
				}
			break; // StateBCom	
			}
			
			case StateComEnd:{
				if (c=='/'){
					state=StateStart;
				}
				else if (c=='*'){
					state=StateComEnd;
				}
				else {
					if(c==EOF){
						fprintf(stderr,"Line %d: Missing block comment terminating character.\n",*line-1);
						setErr(LEXICAL_ERR);
						token.state=StateEnd;						
						state=StateEnd;
					}
					else {
						if(c=='\n')
						++*line;
						state=StateBCom;
					}
				}
			break; // StateComEnd	
			}
			
			case StateStr:{
				if (c=='"'){
					if(str_err > 0){
						fprintf(stderr,"Line %d: Missing string terminating character.\n",*line-str_err);
						setErr(LEXICAL_ERR);
						str_err=0;
					}
					token.state=StateStr;
					state=StateEnd;
				}
				else if (c<=31){
					token.state=StateStr;
					state=StateStr;
					str_err++;
					if (c=='\n')
					++*line;	
				}
				else if(c=='$'){
					token.state=StateStr;
					state=StateStr;
					setErr(LEXICAL_ERR);
					fprintf(stderr,"Line %d: Unexpected character $ in string.\n",*line);
				}
        else if(c == '\\')
        {
          c = fgetc(source);
          if(c == 'n')
            stringAdd(&str, '\n');
          else if(c == 't')
            stringAdd(&str, '\t');
          else if(c == '\\')
            stringAdd(&str, '\\');
          else if(c == '\"')
            stringAdd(&str, '\"');
          else if(c == '$')
            stringAdd(&str, '$');
          else if(c == 'x')
          {
            // Beru cislo po jednom at muzu lehce ungetnout a break;
            char tmp;
            tmp = fgetc(source);
            if(!isHexa(tmp)) //prvni cislo
            {
              stringAdd(&str, '\\');
              stringAdd(&str, 'x');
              ungetc(tmp, source);
              break;
            }
            int ascii = fromHexa(tmp);

            c = fgetc(source);
            if(!isHexa(c)) // druhe cislo
            {
              stringAdd(&str, '\\');
              stringAdd(&str, 'x');
              stringAdd(&str, tmp);
              ungetc(c, source);
              break;
            }
            ascii = ascii * 16;
            ascii += fromHexa(c);
            stringAdd(&str, ascii);
          }
          else
          {
            stringAdd(&str, '\\');
            ungetc(c, source);
          }
        }
				else {
					stringAdd(&str, c);
				}
			
			break; // StateStr
			}			
			
			case StateIsEQ:{
				if(c=='='){
					state=StateEQ;
				}
				else {
					ungetc(c, source);
					token.state=StateAssign;
					state=StateEnd;
				}
			break; // StateIsEQ
			}
			
			case StateEQ:{
				if(c=='='){
					token.state=StateEQ;
					state=StateEnd;
				}
				else {
					ungetc(c, source);
					token.state=StateEQ;
					state=StateERR;
					fprintf(stderr,"Line %d: Expecting '=' character behind '=='.\n",*line);
					setErr(LEXICAL_ERR);
				}
			break; // StateEQ
			}
			
			case StateIsNEQ:{
				if(c=='='){
					state=StateNEQ;
				}
				else {
					ungetc(c,source);
					token.state=StateNEQ;
					state=StateERR;	
					fprintf(stderr,"Line %d: Expecting '=' character behind '!'.\n",*line);
					setErr(LEXICAL_ERR);
				}
			break; // StateIsNEQ
			}
			
			case StateNEQ:{
				if(c=='='){
					token.state=StateNEQ;
					state=StateEnd;
				}
				else {
					ungetc(c,source);
					token.state=StateNEQ;
					state=StateERR;
					fprintf(stderr,"Line %d: Expecting '=' character behind '!='.\n",*line);
					setErr(LEXICAL_ERR);
				}
			
			break; // StateNEQ
			}
			
			case StateMore:{
				if(c=='='){
					token.state=StateMoreEQ;
					state=StateEnd;
				}
				else {
					ungetc(c, source);
					token.state=StateMore;
					state=StateEnd;
				}
			break; // StateMore;
			}
			
			case StateLess:{
				if(c=='='){
					token.state=StateLessEQ;
					state=StateEnd;
				}
				else if(c=='?'){
					state=StatePHP;
				}
				else {
					ungetc(c, source);
					token.state=StateLess;
					state=StateEnd;
				}
			break; // StateLess;
			}
			
			case StatePHP:{
					if(c=='p'){
						state=StatePHP1;
					}
					else {
						ungetc(c, source);
						token.state=StatePHP;
						state=StateERR;
						fprintf(stderr,"Line %d: Expecting opening tag '<?php'.\n",*line);
						setErr(SYNTAKTIC_ERR);
					}
			break; // StatePHP
			}
			
			case StatePHP1:{
					if(c=='h'){
						state=StatePHP2;
					}
					else {
						ungetc(c, source);
						token.state=StatePHP;
						state=StateERR;
						fprintf(stderr,"Line %d: Expecting opening tag '<?php'.\n",*line);
						setErr(SYNTAKTIC_ERR);
					}
			break; // StatePHP1
			}
			
			case StatePHP2:{
					if(c=='p'){
						if(got_white==1){
							fprintf(stderr,"Line %d: Unexpected white characters or comments, expecting <?php at start of the file.\n",*line);
							setErr(LEXICAL_ERR);
						}
						c=fgetc(source);
						if(!isspace(c) && c!=EOF){
								setErr(SYNTAKTIC_ERR);
								fprintf(stderr,"Line %d: Expected white character behind <?php tag.\n",*line);
								ungetc(c,source);
							}
						token.state=StatePHP;
						state=StateEnd;
					}
					else {
						ungetc(c, source);
						token.state=StatePHP;
						state=StateERR;
						fprintf(stderr,"Line %d: Expecting opening tag '<?php'.\n",*line);
						setErr(SYNTAKTIC_ERR);
					}
			break; // StatePHP2
			}
						
			case StateERR:{
				ungetc(c,source);
				token.data=str.data;
				fin=true;
			break; // StateERR
			}
			
			case StateKeyEnd:{
				ungetc(c,source);
				stringClear(&str);
				token.data=str.data;
				fin=true;
			break; // StateKeyEnd
			}
			
			case StateEnd:{
				ungetc(c,source);
				token.data=str.data;
				fin=true;
			break; // StateEnd
			}
			default:;

		}
		
	}
	return token;
}
