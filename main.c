/*
 * Autory: Jakub Gabčo(teamleader)
 *		   Zdeněk Pečínka
 *		   Adam Jež
 *		   Ján Dedič
 *		   Marek Schmith
 * 
 * Kricí názov: Drages
 *  
 * Popis programu: Interpret pre jazyk IFJ13, ktorý je "podmnožinou" jazyka php.
 * Súbor: main.c
 *
 */
 
/*	Hlavičkové súbory	*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include "terror.h"
#include "syn-anal.h"

int main(int argc,char **argv){
	FILE * source;
	if(argc == 1){
		fprintf(stderr,"Source file not given\n");
		return LEXICAL_ERR;
	}
	if((source = fopen(argv[1],"r"))== NULL){
		fprintf(stderr,"Can't open source file!\n");
		return LEXICAL_ERR;
	}
	parser(source);
	fclose(source);
	int NumErr = getNum();
	if(NumErr != 0){
		fprintf(stderr,"Process terminated with status %d [Error = %d]\n",ErrRet(),NumErr);
	}
	return ErrRet();
}
