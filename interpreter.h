#ifndef __INTERPRETER__
#define __INTERPRETER__

int interpret(tListInstr *l, tSymbolTree * t);
double mathOp(double var1, double var2, int operation);
int strval(tData *var, tData *result);
void boolval(tData *var, tData *result);
int doubleval(tData *var, tData *result);
void intval(tData *var, tData *result);
void clearLocal(tSymbolTree *ptr, TSStack *stack);
#endif
