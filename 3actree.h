/************************************/
/*           3actree.h              */  
/************************************/

#ifndef __3ACTREE__
#define __3ACTREE__

#include "tstring.h"
#include "ial.h"
#include "tstack.h"

typedef struct sLabel{
  int label;  //jump number of the label
  char *name; //name of the label
}*tLabel; 


typedef struct TA3ac{
  tTreeItemPtr result;
  void *op1;
  void *op2;
  int operation;
  void *jump;
  tLabel label;
}TA3ac;


typedef struct tListItem {                /* prvek dvouosmerne vazaneho seznamu */ 
  TA3ac* ins;
  struct tListItem *nextptr;          /* ukazatel na nasledujici prvek seznamu */
} *tLItem;

typedef struct
{
  tLItem first;  // ukazatel na prvni prvek
  tLItem last;   // ukazatel na posledni prvek
  tLItem active; // ukazatel na aktivni prvek
} tListInstr;



tLItem  listGetData(tListInstr *L);
void *listGetPointerLast(tListInstr *L);
void listGoto(tListInstr *L, tLItem gotoInstr);
void listNext(tListInstr *L);
void listFirst(tListInstr *L);
void listInsertLast(tListInstr *L, TA3ac* ins);
void listFree(tListInstr *L);
void listInit(tListInstr *L);

void printList(tListInstr *L);
void printListItem(tTreeItemPtr ptr);

void assNodesInit();
void assNodesFree();
void ass_nodeInsert(ASSItemPtr *T, int operation,tTreeItemPtr operand);
tTreeItemPtr createTmpVar(int rank, tSymbolTree *T);

enum operator{
  TO_ADD, // 0 TypeOperation Add - soucet '+'
  TO_SUB, // 1 Sub - odcitani '-'
  TO_MUL, // 2 Mul - nasobeni '*'
  TO_DIV, // 3 Div - deleni '/'
  TO_CAT, // 4 Concatenate - konkatenace '.'
  
  TO_ASS, // 5 Assign - prirad '='
  
  TO_EQ,     // Equal - rovnost '==='
  TO_NQ,     // Not equal - nerovnost '!=='
  TO_GREAT,  // Greater - vetsi '>'
  TO_LESS,   // Less - mensi '<'
  TO_GEQ,    // Greater equal - vetsi nebo rovno '>='
  TO_LEQ,    // Less equal -  mensi nebo rovno '<='
  
  
  TO_BVAL,   // Boolval - prevede vsechno na bool
  TO_DVAL,   // Doubleval -prevede vsechno na double
  TO_IVAL,   // Intval - prevede vsechno na int
  TO_SVAL,   // Strval - prevede vsechno na retezec
  
  
  TO_GSTR,   // Getstring - nacte jeden radek z stdin
  TO_PSTR,   // Putstring - vypise stringy na stdout
  TO_SLEN,   // Strlen - vrati pocet znaku stringu
  TO_GSSTR,  // Get-substring - vrati podretezec
  TO_FSTR,   // Findstring - vyhleda prvni vyskyt str v str
  TO_SSTR,   // Sortstring - seradi znaky v str
  
  TO_JMP,    // Jump - skok (podminka, cykl) -> ukaze na label kde treba skocit
  TO_LABEL,  // Label - kam treba skocit
  TO_FFCE,   // JMP FROM FUNCTION 
  TO_FCE,    // Jump na funckiu
  TO_RET     // Return
};

//DOCASNE - jen pro ucely testovani
const char *operators[]={
    [TO_ADD] = "+",
    [TO_SUB] = "-",          
    [TO_ASS] = "=",          
    [TO_EQ] = "===",      
    [TO_MUL] = "*",       
    [TO_DIV] = "/",        
    [TO_CAT] = ".",          
    [TO_NQ] = "!==",          
    [TO_GREAT] = ">",   
    [TO_LESS] = "<",     
    [TO_GEQ] = ">=",    
    [TO_LEQ] = "<=",
    [TO_BVAL] = "BOOLVAL",
    [TO_DVAL] = "DOUBLEVAL",
    [TO_SVAL] = "STRVAL",
    [TO_IVAL] = "INTVAL",
    [TO_GSTR] = "GETSTRING",
    [TO_PSTR] = "PUTSTRING",
    [TO_SLEN] = "STRINGLEN",
    [TO_SSTR] = "SORTSTRING",
    [TO_FSTR] = "FINDSTRING",
    [TO_GSSTR] = "GETSUBSTRING",
    [TO_LABEL] = "LABEL",
    [TO_JMP] = "JUMP",
    [TO_FCE] = "Jump function",
    [TO_FFCE] = "JUMP FROM FUNCTION",
    [TO_RET] = "Return"
};



#endif
