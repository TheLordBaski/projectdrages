#ifndef __TSTRING__
#define __TSTRING__

/*
 * Autory: Jakub Gabčo(teamleader)
 *		   Zdeněk Pečínka
 *		   Adam Jež
 *		   Ján Dedič
 *		   Marek Schmith
 * 
 * Kricí názov: Drages
 *  
 * Popis programu: Interpret pre jazyk IFJ13, ktorý je "podmnožinou" jazyka php.
 * Súbor: string.h
 *
 */

/*
 * Data ukazatel na postupnost charov
 * 
 *
 */ 
typedef struct{
 	char *data;
 	int length;
 	int allocated;
}string;

int stringCmp(string* str1,string* str2);
int string2int(string* str);
void stringClear(string* str);
int stringAdd(string* str,char c);
void stringFree(string* str);
int stringCreate(string* str);
int stringCopy(string* source,string* destination);
void stringAddLenght(string *str);
int stringLen(string* str);
int stringConstCmp(string* str1, char* str2);
int string2double(string* str, double* val);
int stringCreateWithLenght(string* str);
int strCat(string* str1, string* str2, string* result);
int isHexa(char c);
int fromHexa(char c);
#endif
