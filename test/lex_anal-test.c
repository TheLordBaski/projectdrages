/*
Vypis prijatych tokenu s jejich daty
*/


#include <stdio.h>
#include <stdlib.h>

#include "../lex_anal.h"


//Slouzi pro citelnejsi vypis, misto cisla state tak i nazev
const char *States[]={
    [StateStart] = "StateStart",
    [StateID] = "StateID",           // 01 - Identifikátor
    [StateInt] = "StateInt",          // 02 - Integer
    [StateDouble] = "StateDouble",       // 03 - Double
    [StatePlus] = "StatePlus",         // 04 - +
    [StateMinus] = "StateMinus",        // 05 - -
    [StateLP] = "StateLP",           // 06 - (
    [StateRP] = "StateRP",           // 07 - )
    [StateMore] = "StateMore",         // 08 - >
    [StateLess] = "StateLess",         // 09 - <
    [StateLessEQ] = "StateLessEQ",       // 10 - <=
    [StateMoreEQ] = "StateMoreEQ",       // 11 - >=
    [StateComma] = "StateComma",          // 12 - Čárka
    [StateMul] = "StateMul",          // 13 - *
    [StateDiv] = "StateDiv",          // 14 - /
    [StateDol] = "StateDol",          // 15 - $
    [StateEQ] = "StateEQ",           // 16 - ===
    [StateNEQ] = "StateNEQ",          // 17 - !==
    [StateEnd] = "StateEnd",          // 18 - Koncovy stav
    [StateOr] = "StateOr",           // 19 - ||
    [StateAnd] = "StateAnd",          // 20 - &&
    [StateSemiColon] = "StateSemiColon",    // 21 - ;
    [StateLBracket] = "StateLBracket",     // 22 - [
    [StateRBracket] = "StateRBracket",     // 23 - ]
    [StateVar] = "StateVar",      // 24 - Neukonceny string
    [StateKeyEnd] = "StateKeyEnd",        // 25 - Klíčové slovo
	[StateERR] ="StateERR",		   // 26 - Chybový stav
	[StateIsExp] ="StateIsExp",		   // 27 - Kontrola znaménka exponentu
	[StateExp] ="StateExp",          // 28 - Číslo s exponentem
	[StateLCom] ="StateLCom",         // 29 - Řádkový komentář
	[StateBCom] ="StateBCom",         // 30 - Blokový komentář
	[StateIsDouble] ="StateIsDouble",     // 31 - Kontrola znaku za desetinnou čárkou
	[StateComEnd] ="StateComEnd",       // 32 - Ukonceni blokoveho komentare
	[StateQM] ="StateQM",           // 33 - Dvojite uvozovky
	[StateStr] ="StateStr",          // 34 - Čtení řetězce
	[StateIsExp2] ="StateIsExp2",       // 35 - Kontrola číslice po +- v exponentu
	[StateLBrace] ="StateLBrace",       // 36 - Levá složená závorka
	[StateRBrace] ="StateRBrace",       // 37 - Pravá složená závorka
	[StateIsEQ] ="StateIsEQ",         // 38 - Kontrola počtu '='
	[StateAssign] ="StateAssign",       // 39 - Přiřazení
	[StateIsNEQ] ="StateIsNEQ",        // 40 - Mezistav nerovnosti  
	[StateDot] ="StateDot",        // 41 - Tečka
	[StatePHP] ="StatePHP",		   // 42 - <?
	[StatePHP1] ="StatePHP1",         // 43 - <?p
	[StatePHP2] ="StatePHP2",         // 44 - <?ph    
	[StateElse] ="StateElse",         // 45 - Else
	[StateFunction] ="StateFunction",     // 46 - Function
	[StateIf] ="StateIf",           // 47 - If
	[StateReturn] ="StateReturn",       // 48 - Return
	[StateWhile] ="StateWhile",        // 49 - While
	[StateTrue] ="StateTrue",         // 50 - True
	[StateFalse] ="StateFalse",        // 51 - False
	[StateNull] ="StateNull",         // 52 - Null
  [StateBoolVal] = "StateBoolVal",      // 53 - Vstavana funkcia
  [StateDoubleVal] = "StateDoubleVal",    // 54 - -||-
  [StateIntVal] = "StateIntVal",       // 55 - -||-
  [StateStrVal] = "StateStrVal",       // 56 - -||-
  [StateGetStr] = "StateGetStr",       // 57 - -||-
  [StatePutStr] = "StatePutStr",       // 58 - -||-
  [StateStrLen] = "StateStrLen",       // 59 - -||-
  [StateGetSBStr] = "StateGetSBStr",     // 60 - -||-
  [StateFindStr] = "StateFindStr",     // 61 - -||-
  [StateSortStr] = "StateSortStr"      // 62 - -||-

};

int main(int argc, char **argv)
{
  if(argc != 2)
    return 1;
    
  FILE *soubor = fopen(argv[1],"r");;
  
  Token token;
  int line=1;
  while(1)
  {
    token = getToken(soubor,&line);
    if(token.state == StateEnd){
      printf("Prijal jsem token %s (c.%d) \n", States[token.state], token.state);
      break;
    }
    
    printf("Prijal jsem token %s (c.%d) s obsahem \"%s\"\n", States[token.state], token.state, token.data);
    getchar();
  
  }

  fclose(soubor);
  return 0;

}
