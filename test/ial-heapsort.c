#include <stdio.h>
#include <string.h>
#include "../ial.h"

void testHeapSort()
{
  char text[20];
  scanf("%s",text);
  printf("Puvodni retezec: %s\n",text);
  int a = (strlen(text) - 1);
  heapSort(text,a);
  printf("Setrideny retezec: %s\n", text);
}

int main()
{
  testHeapSort();
  return 0;
}
