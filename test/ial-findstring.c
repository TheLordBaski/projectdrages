#include <stdio.h>
#include <stdlib.h>

#include "../ial.h"
#include "../tString.h"


int main()
{

  string *match, *str;
  match = (string *) malloc(sizeof(string));
  str = (string *) malloc(sizeof(string));  
  
  char text1[1000], text2[1000];
  int j = 0;
  
  printf("Napis retezec ve kterem se ma vyhledavat, max 1000 znaku!\n");
  while((text1[j] = getchar()) != '\n' && text1[j] != EOF)j++;
  
  text1[j] = '\0';
  
  j = 0;
  printf("Napis retezec ktery se bude vyhledavat, max 1000 znaku!\n");
  while((text2[j] = getchar()) != '\n' && text2[j] != EOF)j++;
  
  text2[j] = '\0';
  
  stringCreate(match); 
  stringCreate(str);
  int stringAdd(string* str,char c);

  for(int i = 0; text1[i] != '\0'; i++)
  {
    stringAdd(str, text1[i]);
  }

  for(int i = 0; text2[i] != '\0'; i++)
  {
    stringAdd(match, text2[i]);
  }

  int result = findString(str ,match);
  printf("Retezec jsem nasel/nenasel na indexu %d \n", result);
  return 0;
}
