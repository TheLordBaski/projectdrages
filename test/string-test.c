/*
 * Testovací súbor pre string.c
 */

#include <stdio.h>
#include "../tstring.h"
#include <string.h>

void printString(string str){
	printf("\nVypíšeme si hodnoty stringu\n");
	printf("Dĺžka: %d\n",str.length);
	printf("Reťazec: %s\n",str.data);
	printf("Veľkost dát: %d\n\n",str.allocated);
} 

int main(){
	printf("Vytvoríme si prvý string\n");
	string str1;
	stringCreate(&str1);
	printString(str1);
	printf("Vložíme si tam nejaké dáta\n");
	for(int i = 0; i < 5; i++){
		stringAdd(&str1,'a'+i);
	}
	printString(str1);
	char cmpStr[] = "abcde";
	printf("Porováme si náš string a statický string\n");
	int retVal;
	retVal = stringConstCmp(&str1,cmpStr);
	printf("Návratová hodnota je %d\n\n",retVal);
	printf("Vytvoríme si druhý string");
	string str2;
	stringCreate(&str2);
	printString(str2);
	printf("Vložíme si tam nejaké dáta\n");
	for(int i = 0; i < 5; i++){
		stringAdd(&str2,'d'+i);
	}
	printString(str2);
	printf("Porovnáme si naše 2 vytvorené stringy\n");
	retVal = stringCmp(&str1,&str2);	
	printf("Návratová hodnota je %d\n\n",retVal);

	printf("Vymažeme si obsah prvého reťazca\n");
	stringClear(&str1);
	printString(str1);
	printf("Skopírujeme si obsah druhého reťazca do prvého");
	stringCopy(&str2,&str1);
	printString(str1);
	printf("Vytvoríme si tretí string");
	string str3;
	stringCreate(&str3);
	printString(str3);
	printf("Skúsime skopírovať sem väčší reťazec ako sa tam zmestí\n");
	stringCopy(&str2,&str3);
	printString(str3);
	printf("Vyčistíme tretí string a vložíme doňho číselnu hodnotu\n");
	stringClear(&str3);	
	printString(str3);
	for(int i = 0; i < 3 ;i++)
		stringAdd(&str3,'0'+i);	
	printString(str3);
	printf("Skonvertujeme číslo do integeru a double\n");
	double val1 = string2double(&str3);
	int val2 = string2int(&str3);
	printf("Hodnoty čísiel sú \n \tdouble:  %g\n\tinteger: %d\n\n",val1,val2);
	printf("Nakoniec uvolníme stringy\n");
	stringFree(&str1);
	stringFree(&str2);
	stringFree(&str3);
	return 0;
}
//Preklad v main zložke projektu pomocou gcc -std=c99 -pedantic -Wall -Wextra test/string-test.c string.c error.c -o sTest