#include "tstack.h"

////////////////////// Zasobnik stavu ZACATEK ////////////////////////////
void SSInit(tStack* stack){
	stack->top=NULL;
}

void TSSInit(TSStack* stack){
  stack->top=NULL;
}

void SSPush(tStack* stack, Sstate state){
  tSSItem new;

  if( (new = (tSSItem) malloc(sizeof(struct SSItem))) == NULL )
  {
    //AllocERR();
    fprintf(stderr,"Nepodarila se alokace nove polozky zasobniku stavu.\n");
  }
  else
  {
    new->state = state;
    if (stack->top == NULL) {
      new->lptr = NULL;
    }
    else {
      new->lptr = stack->top;
    }
    stack->top = new;
  }	
}

void TSSPush(TSStack* stack, void *ptr){
  tTSItem new;

  if( (new = (tTSItem) malloc(sizeof(struct TSSItem))) == NULL )
  {
    //AllocERR();
    fprintf(stderr,"Nepodarila se alokace nove polozky zasobniku stavu.\n");
  }
  else
  {
    new->ptr = ptr;
    if (stack->top == NULL) {
      new->ptrnext = NULL;
    }
    else {
      new->ptrnext = stack->top;
    }
    stack->top = new;
  } 
}

int SSEmpty(tStack* stack)
{
  if ( stack->top == NULL )
  {
    return true;
  }
  else
  {
    return false;
  }
}

int TSSEmpty(TSStack* stack)
{
  if ( stack->top == NULL )
  {
    return true;
  }
  else
  {
    return false;
  }
}


void SSPop(tStack* stack)
{
  if ( !SSEmpty(stack) )
  {
    tSSItem temp = stack->top;
    stack->top = temp->lptr;
    free(temp);
  }
}

void TSSPop(TSStack* stack)
{
  if ( !TSSEmpty(stack) )
  {
    tTSItem temp = stack->top;
    stack->top = temp->ptrnext;
    free(temp);
  }
}


void SSTop(tStack* stack, Sstate* state)
{
  if( !SSEmpty(stack) )
  {
    tSSItem temp=stack->top;
    *state=temp->state;
  }
}


void* TSSTop(TSStack* stack)
{
  if( !TSSEmpty(stack) )
  {
    return stack->top->ptr;
  }
  return NULL;
}


Sstate SSFindTerm(tStack* stack){
	if(!SSEmpty(stack)){
		tSSItem tmp; //premmena po ktorej sa budeme posuvat 
		tmp = stack->top;
		while(tmp != NULL){
			if(isTerminal(tmp->state))
				return tmp->state;
			else
				tmp = tmp->lptr;
		}
	}
	return StateERR;
}

int isTerminal(Sstate s){
		if(s==StatePlus || s==StateMinus || s==StateMul || s==StateDiv || s==StateDot || s==StateLess || s==StateMore || s==StateMoreEQ || s==StateLessEQ || s==StateNEQ || s==StateEQ || s==StateRP || s==StateLP || s==StateDol || s==StateVar||s==StateInt||s==StateDouble||s==StateExp||s==StateTrue||s==StateFalse||s==StateStr||s==StateNull)
			return true;
		else 
			return false;
	}

void SSInsert(tStack* stack){
  if(isTerminal(stack->top->state))//navrchuje terminal
    SSPush(stack,StateLCom);
  else{
    SSPop(stack);
    SSPush(stack,StateLCom);
    SSPush(stack,E);
  }
}

void printStack(tStack* stack){
  if(stack->top == NULL){
    printf("________________\n");
    printf("|               |\n");
    printf("________________\n");
    return;
  }
  tSSItem tmp = stack->top;
  printf("________________\n");
  do{
    
    printf("|   %s   |   \n",States[tmp->state]);
    printf("________________\n");
    if(tmp->lptr != NULL)
      tmp = tmp->lptr;
    else 
      tmp = NULL;
  }while(tmp != NULL);
  return;
}

void printSTStack(TSStack* stack){
  ASSItemPtr ptr;
  if(stack->top == NULL){
    printf("_________\n");
    printf("|       |\n");
    printf("_________\n");
    return;
  }
 tTSItem tmp = stack->top;
  printf("_________\n");
  do{
    ptr =tmp->ptr ;
    printf("|   %s   |   \n",ptr->operand == NULL ? "operace" : "operand");
    printf("_________\n");
    if(tmp->ptrnext != NULL)
      tmp = tmp->ptrnext;
    else 
      tmp = NULL;
  }while(tmp != NULL);
  return;
}

void SSFree(tStack* stack){
  while(!SSEmpty(stack)){
    SSPop(stack);
  }
}
void freeSTstack(TSStack *stack){
  if(!stack->top){
    return;
  }
  while(!TSSEmpty(stack)){
    TSSPop(stack);
  }
}
//////////////////////// Zasobnik stavu KONEC //////////////////////////
