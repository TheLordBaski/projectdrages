/*
 * Autory: Jakub Gabčo(teamleader)
 *		   Zdeněk Pečínka
 *		   Adam Jež
 *		   Ján Dedič
 *		   Marek Schmith
 *
 * Kricí názov: Drages
 *
 * Popis programu: Interpret pre jazyk IFJ13, ktorý je "podmnožinou" jazyka php.
 * Súbor: string.c
 *
 */
#include <malloc.h>
#include "tstring.h"
#include "terror.h"
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

const size_t MIN_LENGTH = 4; //Minimálna dlžka podla ktorej sa bude allokovať dlžka reťazca

/* 
 *Funkcia stringCreate allocuje miesto pre reťazec o min dĺžke MIN_LENGTH
 * @param - Reťazec, ktorý budeme neskôr používať
 */

int stringCreate(string* str){
	str->length = 0;
 	if((str->data = (char*)malloc(MIN_LENGTH)) == NULL){
 		AllocERR();
 		setErr(INTERNAL_ERR);
 		return 1;
 	}
 	str->data[0] = '\0';
 	str->allocated = MIN_LENGTH;
 	return 0;
}

/* 
 *Funkcia stringCreateWithLenght allocuje miesto pre reťazec dĺžke str->length
 * @param - Reťazec, ktorý budeme neskôr používať
 */

int stringCreateWithLenght(string* str){
 	if((str->data = (char*)malloc(str->allocated)) == NULL){
 		AllocERR();
 		setErr(INTERNAL_ERR);
 		return 1;
 	}
 	str->data[0] = '\0';
 	str->length = 0;
 	return 0;
}

void stringAddLenght(string *str){
  int i;
  for(i = 0; str->data[i]!= '\0'; i++);
  str->length = i;
}

void stringFree(string* str){
	free(str->data);
}


//Funcia rozpozna ci je volna pamet pre nový znak ktorý sa pridá do reťazca 
int stringAdd(string* str,char c){
	if((str->length+1) >= str->allocated){
		if((str->data = (char*)realloc(str->data,str->allocated+MIN_LENGTH)) == NULL){
			AllocERR();
			setErr(INTERNAL_ERR);
			return 1;
		}
		str->allocated += MIN_LENGTH;
	}
	str->data[str->length] = c;
	str->length+=1;
	str->data[str->length] = '\0';

	return 0;
}

//Reallokacia na MIN_LENGTH sa pre usporu práce s pamäťou nerobí 
void stringClear(string* str){
	str->data[0] = '\0';
	str->length = 0;
	return;
}

int strCat(string* str1, string* str2, string* result)
{	
	if(stringCreate(result))
		return 1;
	
	if(str1 == NULL || str2 == NULL)
		return 1;

	for(int i = 0; i < str1->length; i++)
	{
		if(stringAdd(result, str1->data[i]))
			return 1;
	}

	for(int i = 0; i < str2->length; i++)
	{
		if(stringAdd(result, str2->data[i]))
			return 1;
	}
  	
	return 0;
} 

int stringConstCmp(string* str1, char* str2){
	return strcmp(str1->data,str2);
}

int stringCmp(string* str1,string* str2){
	return strcmp(str1->data,str2->data);
}

int stringCopy(string* source,string* destination){
  // Ukazuji na stejny string -> nedelat nic
  if(source == destination || source == NULL || destination == NULL)
    return 1;

	if(source->allocated > destination->allocated){
		if((destination->data = (char*)realloc(destination->data,source->allocated)) == NULL){
			AllocERR();
			setErr(INTERNAL_ERR);
			return 2;
		}
		destination->allocated = source->allocated;
	}

	destination->length = source->length;
	strcpy(destination->data,source->data);
	return 0;
}

int stringLen(string* str){
	return str->length;
}
int string2double(string* str, double* val){
	double number = 0.0;
	double decimal = -1.0;
	double exponent = -1.0;
	double dec_div = 10.0;
	bool exp_sign=true;
	bool is_start = true;
	bool is_decimal = false;
	bool is_sign_exponent = false;
	bool is_exponent = false;
	for(int i = 0; i <= str->length; i++){
		if(is_start && isspace(str->data[i])){	// zahazovani bilych znaku na zacatku
			continue;
		}
		if(isdigit(str->data[i]) && !is_decimal && !is_exponent && !is_sign_exponent){	
			number = number * 10;
			number += str->data[i] - '0';
			is_start = false;
			continue;
		}
		if(!is_decimal && !is_exponent && !is_sign_exponent && str->data[i]=='.' && !is_start){
			is_decimal = true;
			continue;
		}
		if(!is_decimal && !is_exponent && !is_sign_exponent && (str->data[i]=='e' || str->data[i]=='E')){
			is_sign_exponent = true;
			continue;
		}
		if(isdigit(str->data[i]) && is_decimal && !is_exponent && !is_sign_exponent){
			if(decimal == -1.0)
				decimal = 0.0;
			decimal = decimal + (str->data[i] - '0')/dec_div;
			dec_div = dec_div * 10;
			continue;
		}	
		if(is_decimal && (str->data[i]=='e' || str->data[i]=='E') && decimal !=-1.0){
			is_sign_exponent = true;
			is_decimal = false;
			continue;
		}		
		if(is_sign_exponent && str->data[i]=='+'){
			is_sign_exponent = false;
			is_exponent = true;
			continue;
		}
		if(is_sign_exponent && str->data[i]=='-'){
			exp_sign = false;
			is_sign_exponent = false;
			is_exponent = true;
			continue;
		}
		if(is_sign_exponent && isdigit(str->data[i])){
			is_sign_exponent = false;
			is_exponent = true;
			exponent = str->data[i] - '0';
			continue;
		}
		if(is_exponent && isdigit(str->data[i])){
			if(exponent == -1.0)
				exponent =0;
			exponent = exponent * 10;
			exponent += str->data[i] - '0';	
			continue;
		}
		if (is_start){
			*val=0.0;
			return E_OK;
			}
		else if (!is_decimal && !is_exponent && !is_sign_exponent && number!=-1){
			*val=number;
			return E_OK;
		}
		else if(is_decimal && decimal!=-1.0){
			*val=number+decimal;
			return E_OK;
		}
		else if((is_exponent || is_sign_exponent) && exponent != -1.0){
			int exp=1;
				for(int j=0;j<exponent;j++){
						exp=exp * 10;
					}
			if(decimal!=-1.0)
				number = number + decimal;
			if(!exp_sign)
				*val=number / exp;
			else
				*val=number * exp;		
			return E_OK;	
		}
		else {
			return DOUBLEVAL_ERR;
		}	
	}	
	*val = number;
	return E_OK;
}

int string2int(string* str){
  //melo by se resit konecnym automatem, ale v nem se ja neorientuju
  int number = 0;
  bool is_start = true;
  for(int i = 0; i <= str->length; i++)
  {
    if(is_start && isspace(str->data[i]))
      continue;
    else if(isdigit(str->data[i]))
    {
      number = number * 10;
      number += str->data[i] - '0';
      is_start = false;
    }
    else
      return number;
  }
	return number;
}

int isHexa(char c)
{
  if((c >= '0' && c <= '9') ||
     (c >= 'a' && c <= 'f') ||
     (c >= 'A' && c <= 'F'))
    return 1;
  return 0;
}

int fromHexa(char c)
{
  if(c >= '0' && c <= '9')
    return (int)c - '0'; 
  else if(c >= 'A' && c <= 'F')
    return (int)c - 'A' + 10;
  else if(c >= 'a' && c <= 'f')
    return (int)c - 'a' + 10;  
  
  return 0;
}
