CFLAGS=-std=c99 -Wall -pedantic -g -Wextra -Wl,--allow-multiple-definition -g -o2
FILE=main.c lex_anal.c error.c string.c syn-anal.c ial.c tstack.c 3actree.c interpreter.c
BIN=project
CC=gcc
RM=rm -f


ALL: 
	$(CC) $(CFLAGS) $(FILE) -o $(BIN) 

clean:
	$(RM) *.o $(BIN)

string:
	$(CC) $(CFLAGS) test/string-test.c string.c error.c -o sTest
lex:
	$(CC) $(CFLAGS) test/lex_anal-test.c string.c error.c lex_anal.c -o lexTest

ial:
	$(CC) $(CFLAGS) test/ial-test.c ial.c error.c -o sIal
heapsort:
	$(CC) $(CFLAGS) test/ial-heapsort.c ial.c -o HS-test
findstring:
	$(CC) $(CFLAGS) test/ial-findstring.c ial.c string.c error.c -o FS-test
