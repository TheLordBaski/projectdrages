#include "tstring.h"
#include "ial.h"
#include "3actree.h"
#include "interpreter.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//nevim jak bude vypadat zatim
// vyuzite neexistujici vestavene funkce
// strcat - spoji dva retezce


int interpret(tListInstr *L, tSymbolTree *t)
{
  //Zasobnik pro zprovozneni funkci
  TSStack stack;
  TSStack *argStack;
  TSSInit(&stack);
    
  //Nastavim aktivitu na prvni instrukci
  listFirst(L);
  tLItem item;
  tTreeItemPtr op1, op2, result;
  int instruction;
  
  //docasne promenne
  tTreeItem itemTmp1, itemTmp2;
  tTreeItemPtr ptrTmp1, ptrTmp2;
  tSymbolTree *treeTmp1 = NULL, *treeTmp2;
  char *strTmp;

while((item = listGetData(L)) != NULL)
{
  listNext(L);
  strTmp = NULL;
  op1 = item->ins->op1;
  op2 = item->ins->op2;
  result = item->ins->result;
  instruction = item->ins->operation;
  
  //Resime promenne ve funkci
  if(treeTmp1 != NULL) // Jestli jsme ve funkci
  {
    if(op1 != NULL)
      op1 = treeSearch(treeTmp1,op1->key);
    if(op2 != NULL)
    op2 = treeSearch(treeTmp1,op2->key);
    if(result != NULL)
      result = treeSearch(treeTmp1,result->key);
      
    //spesl instrukce getsubstring
    if(instruction == TO_GSSTR)
    {
      ptrTmp1 = item->ins->jump;
      item->ins->jump = treeSearch(treeTmp1,ptrTmp1->key);
    }
  }
  
  //Overeni nedeklarovane promenne
  if((op1 != NULL && op1->data.varType == TYPE_VAR) || 
     (op2 != NULL && op2->data.varType == TYPE_VAR))
     {
      clearLocal(treeTmp1, &stack);
      return UNDEC_VAR_ERR;
     }

  if(result != NULL && result->data.varType == TYPE_STRING)
      strTmp = result->data.varValue.string_var.data;
switch(instruction)
{
  // SCITANI
  case TO_ADD:
  // ODCITANI
  case TO_SUB:
  // NASOBENI
  case TO_MUL:
    if(op1->data.varType == TYPE_INT && 
       op2->data.varType == TYPE_INT)
    {
      result->data.varType = TYPE_INT;
      if(instruction == TO_ADD)      // pricitani
        result->data.varValue.int_var = op1->data.varValue.int_var + 
                                        op2->data.varValue.int_var;
      else if(instruction == TO_SUB) // odcitani
        result->data.varValue.int_var = op1->data.varValue.int_var -
                                        op2->data.varValue.int_var;
      else // zbyva nasobeni
        result->data.varValue.int_var = op1->data.varValue.int_var *
                                        op2->data.varValue.int_var;
    }
    else if((op1->data.varType == TYPE_DOUBLE || // Jeden z operandu je double 
             op1->data.varType == TYPE_INT) &&
            (op2->data.varType == TYPE_DOUBLE ||
             op2->data.varType == TYPE_INT ))
    {
      result->data.varType = TYPE_DOUBLE; // Vysledek musi byt typu double
      
      if(op1->data.varType == TYPE_INT) // Prvni operand je int
      {
        doubleval(&(op1->data), &(itemTmp1.data));
        ptrTmp1 = &itemTmp1;
      }
      else
        ptrTmp1 = op1;
      
      if(op2->data.varType == TYPE_INT) // Druhy operand je int
      {
        doubleval(&(op2->data), &(itemTmp2.data));
        ptrTmp2 = &itemTmp2;  
      }
      else
        ptrTmp2 = op2;
      
      result->data.varValue.double_var = 
        mathOp(ptrTmp1->data.varValue.double_var, ptrTmp2->data.varValue.double_var, instruction);
    }
    else
    {
      clearLocal(treeTmp1, &stack);
      return COMPAT_ERR;//NEJAKEJ ERROR
    }
    break;
    
  // DELENI
  case TO_DIV:
    if((op1->data.varType == TYPE_DOUBLE || // Oba jsou double nebo int
        op2->data.varType == TYPE_DOUBLE) ||
       (op1->data.varType == TYPE_INT ||
        op2->data.varType == TYPE_INT ))
    {
      result->data.varType = TYPE_DOUBLE; // Vysledek je vzdy double
      
      if(op1->data.varType == TYPE_INT) // Prvni operand je int
      {
        doubleval(&(op1->data), &(itemTmp1.data));
        ptrTmp1 = &itemTmp1;
      }
      else
        ptrTmp1 = op1;
      
      if(op2->data.varType == TYPE_INT)
      {
        doubleval(&(op2->data), &(itemTmp2.data));
        ptrTmp2 = &itemTmp2;  
      }
      else
        ptrTmp2 = op2;
      
      if(ptrTmp2->data.varValue.double_var == 0.0){
        clearLocal(treeTmp1, &stack);
        return DIV_ZERO_ERR;
      }
      // Samotne deleni
      result->data.varValue.double_var = ptrTmp1->data.varValue.double_var 
                                          / ptrTmp2->data.varValue.double_var;
    }
    else
    {
      clearLocal(treeTmp1, &stack);
      return COMPAT_ERR;//NEJAKEJ ERROR
    }
      
    break;
    
  // KONKATENACE
  case TO_CAT:
    //if(result != NULL && result->data.varType == TYPE_STRING)
    //  strTmp = result->data.varValue.string_var.data;
   //op2 -> prvni operand musi byt string
    if(op1->data.varType != TYPE_STRING)
    {
      clearLocal(treeTmp1, &stack);
      return COMPAT_ERR;
    }
    
    if(op2->data.varType != TYPE_STRING)
    {
      strval(&(op2->data), &(itemTmp1.data));
      ptrTmp1 = &itemTmp1;
    }
    else
      ptrTmp1 = op2;
  
      
    if(strCat(&(op1->data.varValue.string_var),
       &(ptrTmp1->data.varValue.string_var),
       &(result->data.varValue.string_var)))
    {
      clearLocal(treeTmp1, &stack);
      return E_MALLOC; //ERROR
    }
    //Uvolnime pamet docasneho itemu
    if(op2->data.varType != TYPE_STRING)
      free(ptrTmp1->data.varValue.string_var.data);
    result->data.varType = TYPE_STRING;
    break;
    
  // PRIRAZENI
  case TO_ASS:
    if(op1 == result) //ukazuji na stejny prvek, nic nedelat
      break;
    
    if(op1->data.varType == TYPE_INT)
      result->data.varValue.int_var = op1->data.varValue.int_var;
    else if(op1->data.varType == TYPE_DOUBLE)
      result->data.varValue.double_var = op1->data.varValue.double_var;
    else if(op1->data.varType == TYPE_STRING)
    {
      if(stringCreate(&(result->data.varValue.string_var))){
        clearLocal(treeTmp1, &stack);
        return E_MALLOC; //ERROR
      }
          
      if(stringCopy(&(op1->data.varValue.string_var), &(result->data.varValue.string_var)))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC; // nejakej malloc error
      }
    }
    else if(op1->data.varType == TYPE_BOOL)
      result->data.varValue.bool_var = op1->data.varValue.bool_var;

    result->data.varType = op1->data.varType;
    break;
    
  // ROVNOST
  case TO_EQ:
  // NEROVNOST
  case TO_NQ:
    result->data.varType = TYPE_BOOL;
    if(op1->data.varType != op2->data.varType)
    {
      result->data.varValue.bool_var = false;
    }
    else if (op1->data.varType == TYPE_INT) // Oba jsou int
      result->data.varValue.bool_var = (op1->data.varValue.int_var == op2->data.varValue.int_var);
    else if (op1->data.varType == TYPE_DOUBLE) // Oba jsou double
      result->data.varValue.bool_var = (op1->data.varValue.double_var == op2->data.varValue.double_var);
    else if (op1->data.varType == TYPE_STRING) // Oba jsou double
      result->data.varValue.bool_var = !stringCmp(&(op1->data.varValue.string_var), &(op2->data.varValue.string_var));
    else if (op1->data.varType == TYPE_BOOL) // Oba jsou double
      result->data.varValue.bool_var = (op1->data.varValue.bool_var == op2->data.varValue.bool_var);
    else // Oba jsou NULL -> true ?
      result->data.varValue.bool_var = true;
    
    if(instruction == TO_NQ) // operace nerovnost -> znegujeme vysledek
      result->data.varValue.bool_var = !result->data.varValue.bool_var;
    break;
    
  // Vetsi nez
  case TO_GREAT:
  // Mensi nebo rovno
  case TO_LEQ:
    result->data.varType = TYPE_BOOL;
    if(op1->data.varType != op2->data.varType)
    {
      result->data.varValue.bool_var = false;
      break; // Jeste si nejsem jisty
    }
    else if (op1->data.varType == TYPE_INT) // Oba jsou int
      result->data.varValue.bool_var = (op1->data.varValue.int_var > op2->data.varValue.int_var);
    else if (op1->data.varType == TYPE_DOUBLE) // Oba jsou double
      result->data.varValue.bool_var = (op1->data.varValue.double_var > op2->data.varValue.double_var);
    else if (op1->data.varType == TYPE_STRING) // Oba jsou double
      if(stringCmp(&(op1->data.varValue.string_var), &(op2->data.varValue.string_var)) > 0)
        result->data.varValue.bool_var = true;
      else
        result->data.varValue.bool_var = false;
    else if (op1->data.varType == TYPE_BOOL) // Oba jsou double
      result->data.varValue.bool_var = (op1->data.varValue.bool_var > op2->data.varValue.bool_var);// Tady to moc nechapu
    else
      result->data.varValue.bool_var = false;
      
    if(instruction == TO_LEQ) // operace nerovnost -> znegujeme vysledek
      result->data.varValue.bool_var = !result->data.varValue.bool_var;
      
    break;
    
  // Mensi nez
  case TO_LESS:
  // Vetsi nebo rovno
  case TO_GEQ:
    result->data.varType = TYPE_BOOL;
    if(op1->data.varType != op2->data.varType)
    {
      result->data.varValue.bool_var = false;
      break; // Jeste si nejsem jisty
    }
    else if (op1->data.varType == TYPE_INT) // Oba jsou int
      result->data.varValue.bool_var = (op1->data.varValue.int_var < op2->data.varValue.int_var);
    else if (op1->data.varType == TYPE_DOUBLE) // Oba jsou double
      result->data.varValue.bool_var = (op1->data.varValue.double_var < op2->data.varValue.double_var);
    else if (op1->data.varType == TYPE_STRING) // Oba jsou double
    {
      if(stringCmp(&(op1->data.varValue.string_var), &(op2->data.varValue.string_var)) < 0)
        result->data.varValue.bool_var = true;
      else
        result->data.varValue.bool_var = false;
    }
    else if (op1->data.varType == TYPE_BOOL) // Oba jsou double
      result->data.varValue.bool_var = (op1->data.varValue.bool_var < op2->data.varValue.bool_var);// Tady to moc nechapu
    else 
      result->data.varValue.bool_var = false;
      
    if(instruction == TO_GEQ) // operace nerovnost -> znegujeme vysledek
      result->data.varValue.bool_var = !result->data.varValue.bool_var;
    
    break;
  
  // Strval
  case TO_SVAL: 
    if(op1 == NULL)
    {
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
      
    if(strval(&(op1->data), &(result->data)))
    {
      clearLocal(treeTmp1, &stack);
      return E_MALLOC; //error
    }
    break;
  // Boolval
  case TO_BVAL:
    if(op1 == NULL)
    {
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
      
    
    boolval(&(op1->data), &(result->data));
    break;
  
  // Doubleval
  case TO_DVAL:
    if(op1 == NULL)
    {
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
      

    if(doubleval(&(op1->data), &(result->data)))
    {
      clearLocal(treeTmp1, &stack);
      return DOUBLEVAL_ERR;
    }
    break;
    
  // Intval
  case TO_IVAL:
    if(op1 == NULL)
    {
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
      
    intval(&(op1->data), &(result->data));
    break;

  // Getstring
  case TO_GSTR:
    if(stringCreate(&(result->data.varValue.string_var))){
      clearLocal(treeTmp1, &stack);
      return E_MALLOC; // ERROR
    }
    
    char c;
    while((c = getchar()) != '\n' && c != EOF)
    {
      if(stringAdd(&(result->data.varValue.string_var), c))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC; //ERROR
      }
    }
    result->data.varType = TYPE_STRING;
    break;
    
  // Putstring
  case TO_PSTR:
    if(op2 == NULL){
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
      
    if(op2->data.varType != TYPE_STRING){
      if(strval(&(op2->data), &(itemTmp1.data)))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
        
      ptrTmp1 = &itemTmp1;
    }
    else
      ptrTmp1 = op2;

    printf("%s", ptrTmp1->data.varValue.string_var.data);
    if(op2->data.varType != TYPE_STRING)
      stringFree(&(ptrTmp1->data.varValue.string_var));
    
    break;  
    
  // Strlen
  case TO_SLEN:
    if(op1 == NULL){
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
    
    if(op1->data.varType != TYPE_STRING){
      if(strval(&(op1->data), &(itemTmp1.data)))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
        
      ptrTmp1 = &itemTmp1;
    }
    else
      ptrTmp1 = op1;  
    

    result->data.varType = TYPE_INT;
    result->data.varValue.int_var = stringLen(&(ptrTmp1->data.varValue.string_var));


    break;
  
  // Get-substring
  case TO_GSSTR:
    if(op1 == NULL || op2 == NULL || item->ins->jump == NULL){
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }

      
    int int1, int2;
    // 1. Argument
    if(op1->data.varType != TYPE_STRING){
      if(strval(&(op1->data), &(itemTmp1.data))){
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
        
      ptrTmp1 = &itemTmp1;
    }
    else
      ptrTmp1 = op1; 
    
    // 2. Argument
    if(op2->data.varType != TYPE_INT){
      intval(&(op2->data), &(itemTmp2.data));

      int1 = itemTmp2.data.varValue.int_var;
    }
    else
      int1 = op2->data.varValue.int_var; 
      
    // 3. Argument
    // Tento argument se netestoval na deklaraci -> musime otestovat
    ptrTmp2 = (tTreeItemPtr) item->ins->jump;
    if(ptrTmp2->data.varType == TYPE_VAR){
      clearLocal(treeTmp1, &stack);
      return UNDEC_VAR_ERR;
    }
    if(ptrTmp2->data.varType != TYPE_INT){
      intval(&(ptrTmp2->data), &(itemTmp2.data));

      int2 = itemTmp2.data.varValue.int_var;
    }
    else
      int2 = ptrTmp2->data.varValue.int_var; 
      
    //Samotna funkce GET_SUBSTRING
    if(int1 < 0 || int2 < 0 || int1 > int2 
       || int1 >= ptrTmp1->data.varValue.string_var.length
       || int2 > ptrTmp1->data.varValue.string_var.length){
      clearLocal(treeTmp1, &stack);
      return SEMANTIC_ERR;
    }
    
    
    result->data.varType = TYPE_STRING;
    itemTmp2.data.varValue.string_var.allocated = int2 - int1 + 1;
    if(stringCreateWithLenght(&(itemTmp2.data.varValue.string_var))){
      clearLocal(treeTmp1, &stack);
      return E_MALLOC;
    }
    for(int i = int1; i < int2; i++)
    {
      if(stringAdd(&(itemTmp2.data.varValue.string_var), ptrTmp1->data.varValue.string_var.data[i]))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
    }
    
    //Nakopirujeme finalne do resultu (Kopiruju protoze jeden z argumentu mohl byt result)
    result->data.varValue.string_var.allocated = int2 - int1 + 2;
    if(stringCreateWithLenght(&(result->data.varValue.string_var))){
      clearLocal(treeTmp1, &stack);
      return E_MALLOC;
    }
    stringCopy(&(itemTmp2.data.varValue.string_var), &(result->data.varValue.string_var));
     //Uvolnim docasna data
    stringFree(&(itemTmp2.data.varValue.string_var));
    break;
  
  // Findstring
  case TO_FSTR:
    if(op1 == NULL || op2 == NULL)
    {
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
    
    if(op1->data.varType != TYPE_STRING){
      if(strval(&(op1->data), &(itemTmp1.data)))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
        
      ptrTmp1 = &itemTmp1;
    }
    else
      ptrTmp1 = op1; 
      
    if(op2->data.varType != TYPE_STRING){
      if(strval(&(op2->data), &(itemTmp2.data)))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
        
      ptrTmp2 = &itemTmp2;
    }
    else
      ptrTmp2 = op2; 

    result->data.varType = TYPE_INT;
    if(findString(&(ptrTmp1->data.varValue.string_var),&(ptrTmp2->data.varValue.string_var),&(result->data.varValue.int_var)))
    {
      clearLocal(treeTmp1, &stack);
      return E_MALLOC;
    }
    // Uvolnim docasne promenne
    if(op1->data.varType != TYPE_STRING)
      free(ptrTmp1->data.varValue.string_var.data);
    if(op2->data.varType != TYPE_STRING)
      free(ptrTmp2->data.varValue.string_var.data);
    break;
   
  // Sortstring
  case TO_SSTR: 
    if(op1 == NULL)
    {
      clearLocal(treeTmp1, &stack);
      return ARGUMENT_ERR;
    }
    
    if(op1->data.varType != TYPE_STRING){
      if(strval(&(op1->data), &(itemTmp1.data)))
      {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
        
      ptrTmp1 = &itemTmp1;
    }
    else
      ptrTmp1 = op1;  
    
    if(stringCreate(&(result->data.varValue.string_var))) {
      clearLocal(treeTmp1, &stack);
      return E_MALLOC; // ERROR
    }

    stringCopy(&(ptrTmp1->data.varValue.string_var),&(result->data.varValue.string_var));
    heapSort(result->data.varValue.string_var.data, stringLen(&(result->data.varValue.string_var))-1);
    result->data.varType = TYPE_STRING;
    
    if(op1->data.varType != TYPE_STRING)
      free(ptrTmp1->data.varValue.string_var.data);
    break;
  
  //JUMP
  case TO_JMP:
    if(op1 != NULL)
    {
      boolval(&(op1->data), &(itemTmp1.data));
    }

    if(instruction == TO_JMP && (op1 == NULL || !itemTmp1.data.varValue.bool_var))
    {
      listGoto(L, item->ins->jump);
    } 
    break;
    
  //JUMP TO FUNCTION
  case TO_FCE:
    TSSPush(&stack, L->active); // Pushuju nasledujici instrukci at se mam kde vratit
    
    argStack = item->ins->jump;
    //Najdu funkci ve strome
    ptrTmp1 = treeSearch(t, item->ins->label->name); 
    if(ptrTmp1 == NULL){
      TSSPop(&stack);
      clearLocal(treeTmp1, &stack);
      return FUNCTION_ERR;
    }
    
    //Presuneme se na misto funkce
    listGoto(L, (tLItem)ptrTmp1->data.nextArg);
    
    //Ziskame informace o labelu
    item = listGetData(L);
    
    treeTmp2 = treeTmp1;
    TSSPush(&stack, treeTmp1); //pushuju ukazatel na lokalni strom
    TSSPush(&stack, result);    // Pushuju ukazatel na return
    ptrTmp2 = op2; // 1.Argument
    
    // Nakopirujeme si strom
    if((treeTmp1 = malloc(sizeof(tSymbolTree))) == NULL) {
        clearLocal(treeTmp1, &stack);
        return E_MALLOC;
      }
    if(treeCopy((tSymbolTree *)item->ins->jump, treeTmp1)) {
      clearLocal(treeTmp1, &stack);   // Udelam si kopii stromu pro funkci 
      return E_MALLOC;
    }
    
    //Kopiruju hodnoty argumentu
    ptrTmp2 = treeTmp1->root;
    tTreeItemPtr tmp1, tmp2;
    if(argStack != NULL) // neexistuji zadne argument
    {
      tTSItem stackTmp = argStack->top;
      //printf("CHCEME ARGUMENTU %d \n", ptrTmp1->data.varValue.int_var);
      for(int i = 0; i < ptrTmp1->data.varValue.int_var ; i++)
      {
        if(stackTmp == NULL)
        {
          clearLocal(treeTmp1, &stack);
          return ARGUMENT_ERR;
        }
          
        tmp1 = treeSearch(treeTmp1,ptrTmp2->key);
        if(treeTmp2 != NULL) //kopiruju rekurzivne -> musim najit co
          tmp2 = treeSearch(treeTmp2,((tTreeItemPtr)stackTmp->ptr)->key);
        else
          tmp2 = treeSearch(t,((tTreeItemPtr)stackTmp->ptr)->key);

        if(copyData(&tmp2, &tmp1)) //nakopirujeme data
            return E_MALLOC;
            
        stackTmp = stackTmp->ptrnext; //posunu dal argumenty s kterymi byla funkce zavolana
        ptrTmp2 = ptrTmp2->data.nextArg; //posunu dal argumenty funkce
      }
      treeTmp2 = NULL;
    }

    break;
  case TO_FFCE:
  case TO_RET:
    //Vychazime z programu
    if(treeTmp1 == NULL)
      return E_OK;

    //Resime navratovou hodnotu
    ptrTmp1 = (tTreeItemPtr)TSSTop(&stack);
    if(ptrTmp1 != NULL) 
    {
      if(result == NULL)
        ptrTmp1->data.varType = TYPE_NULL;	// return nema operand -> vracim TYPE_NULL
      else
      {
        if(copyData(&result, &ptrTmp1))//nakopirujeme data
        {
          clearLocal(treeTmp1, &stack);
          return E_MALLOC;
        }
      }
    }
    TSSPop(&stack); //popnout vracenou hodnotu  
  
    //Navratime strom se symboly predchazejici funkce
    treeFree(treeTmp1);
    free(treeTmp1); //Uvolnime pouzivany strom
    treeTmp1 = (tSymbolTree *)TSSTop(&stack);
    TSSPop(&stack);  

    listGoto(L, (tLItem)TSSTop(&stack)); // skocime zpatky
    TSSPop(&stack); // popnem navratovou adresu
    break;
  
}
  //Uvolnime retezec, ktery uz nikdo nepotrebuje
  if(strTmp != NULL)
  {
    if(result->data.varType != TYPE_STRING || result->data.varValue.string_var.data != strTmp)
      free(strTmp);
  }
}
    return E_OK;
}
// Pro zprehledneni vykona matematicke operace
double mathOp(double var1, double var2, int operation)
{
  if(operation == TO_ADD) // Scitani
  {
    return var1 + var2;
  }
  else if(operation == TO_SUB) // Odcitani
  {
    return var1 - var2;
  }
  else // Nasobeni
  {
    return var1 * var2;
  }
}

int strval(tData *var, tData *result)
{
  int intTmp; double dblTmp; bool boolTmp;
  
  switch(var->varType)
  {
	case TYPE_STRING:
    if(result->varValue.string_var.data == var->varValue.string_var.data)
      return 0;
	  result->varValue.string_var.allocated = var->varValue.string_var.allocated;
	  if(stringCreateWithLenght(&(result->varValue.string_var)))
        return E_MALLOC;// ERROR
        
	  if(stringCopy(&(var->varValue.string_var),&(result->varValue.string_var)) > 1)
		return E_MALLOC;
    break;
  case TYPE_INT:
    intTmp = var->varValue.int_var; //uchovam pro jistotu
    result->varValue.string_var.allocated = 12; // int max 2,147,483,647 + \0
    if(stringCreateWithLenght(&(result->varValue.string_var)))
    {
      return E_MALLOC;// ERROR
    }
    sprintf(result->varValue.string_var.data,"%d", intTmp);
    stringAddLenght(&(result->varValue.string_var));
    break;
  case TYPE_DOUBLE:
    dblTmp = var->varValue.double_var;
    result->varValue.string_var.allocated = 12; // man 3 printf -> g 6 digits are given :/
    if(stringCreateWithLenght(&(result->varValue.string_var)))
    {
      return E_MALLOC;// ERROR
    }
    sprintf(result->varValue.string_var.data,"%g", dblTmp);
    stringAddLenght(&(result->varValue.string_var));
    break;
  case TYPE_NULL:
    result->varValue.string_var.allocated = 2;
    if(stringCreateWithLenght(&(result->varValue.string_var))) // implicitne se da na 0 index '\0'
    {
      return E_MALLOC;// ERROR
    }
    break;
  case TYPE_BOOL:
    boolTmp = var->varValue.bool_var;
    result->varValue.string_var.allocated = 2;
    if(stringCreateWithLenght(&(result->varValue.string_var)))
    {
      return E_MALLOC;// ERROR
    }
    if(boolTmp)
      stringAdd(&(result->varValue.string_var), '1');
    break;
  }

  result->varType = TYPE_STRING;
  return 0;
}

void boolval(tData *var, tData *result)
{
  if((var->varType == TYPE_INT && var->varValue.int_var != 0) ||
     (var->varType == TYPE_DOUBLE && var->varValue.double_var != 0.0) ||
     (var->varType == TYPE_STRING && var->varValue.string_var.data[0] != '\0'))
    result->varValue.bool_var = true;
  else if(var->varType == TYPE_BOOL)
    result->varValue.bool_var = var->varValue.bool_var;
  else
    result->varValue.bool_var = false;
      
  result->varType = TYPE_BOOL;
}

int doubleval(tData *var, tData *result)
{
  switch(var->varType)
  {
    case TYPE_DOUBLE:
      result->varValue.double_var = var->varValue.double_var;
      break;
    case TYPE_INT:
      result->varValue.double_var = (double) var->varValue.int_var;
      break;
    case TYPE_BOOL:
      if(var->varValue.bool_var)
        result->varValue.double_var = 1.0;
      else
        result->varValue.double_var = 0.0;
      break;
    case TYPE_STRING:
      if(string2double(&(var->varValue.string_var),&result->varValue.double_var))
        return DOUBLEVAL_ERR;
      break;
    case TYPE_NULL:
      result->varValue.double_var = 0.0;
      break;
  }
  
  result->varType = TYPE_DOUBLE;
  return E_OK;
}

void intval(tData *var, tData *result)
{
  switch(var->varType)
  {
    case TYPE_INT:
      result->varValue.int_var = var->varValue.int_var;
      break;
   case TYPE_DOUBLE:
      result->varValue.int_var = (int) var->varValue.double_var;
      break;
   case TYPE_BOOL:
     if(var->varValue.bool_var)
       result->varValue.int_var = 1;
     else
       result->varValue.int_var = 0;
     break;
   case TYPE_STRING:
     result->varValue.int_var = string2int(&(var->varValue.string_var));
     break;
   case TYPE_NULL:
     result->varValue.int_var = 0;
     break;
  }
  
  result->varType = TYPE_INT;
}

void clearLocal(tSymbolTree *ptr, TSStack *stack)
{
  if(ptr == NULL)
    return;
  
  treeFree(ptr);
  free(ptr);
  while(!TSSEmpty(stack))
  {
    TSSPop(stack);  //uvolnime navratovou hodnotu
    if(TSSEmpty(stack))
      return;
    ptr = (tSymbolTree *)TSSTop(stack);
    if(ptr != NULL)
    {
      treeFree(ptr);
      free(ptr);
    }
    TSSPop(stack);  //uvolnime docasny strom
    TSSPop(stack);  //uvolnime navratovou adresu
  }
  
}
