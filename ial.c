#include <stdio.h>
#include <stdlib.h>
#include "terror.h"
#include "ial.h"
#include "tstring.h"
#include <string.h>
#include <malloc.h>
#include <stdbool.h>
#include "terror.h"


// Symbol Tree 

void treeInit(tSymbolTree *T)
// funkce inicializuje strom symbolu
{
  T->root = NULL;
}

tTreeItemPtr treeInsert(tSymbolTree *T, char *key, int varType,char *data)
// funkce vlozi do stromu symbolu novy identifikator
{
  // Vytvorime klic
  char *newkey = malloc(sizeof(char)*(strlen(key)+2));
  if(varType == TYPE_STRING){
    strcpy(newkey,"-");
    strcat(newkey,key);
  }else if(varType == TYPE_FUNCTION){
    strcpy(newkey,"$");
    strcat(newkey,key);
  }
  else
    strcpy(newkey,key);
  
  tTreeItemPtr ptr = NULL;
  // Polozka se stejnym klicem uz ve stromu existuje
  ptr = treeSearch(T, newkey);

  if(ptr == NULL)
    return nodeInsert(&(T->root), newkey, varType,data);
  else
  {
    free(newkey); // ulovnime klic, kdyz uz existuje
    return ptr;
  }
}

tTreeItemPtr nodeInsert(tTreeItemPtr *T, char *key, int varType,char *data)
{
  if(*T == NULL)
  {
    // vlozime do tabulky novou polozku
    if ((*T = malloc(sizeof(struct treeItem))) == NULL)
    {
      AllocERR();
      setErr(INTERNAL_ERR);
      return NULL;
    }

    dataToStruct(T, varType, data);
      
    (*T)->key = key;
    (*T)->data.varType = varType;
    (*T)->data.nextArg = NULL;
    (*T)->RTreeItem = NULL;
    (*T)->LTreeItem = NULL;
    (*T)->PTreeItem = NULL;

  
    return *T;
  }
  else
  {
    if(strcmp(key,((*T)->key)) < 0)
    {
      return nodeInsert(&(*T)->LTreeItem, key, varType, data);
    }
    else
    {
      return nodeInsert(&(*T)->RTreeItem, key, varType,data);
    }
  }
}

tTreeItem *treeSearch(tSymbolTree *T, char *key)
// pokud se dana polozka s klicem key ve stromu symbolu nachazi,
// funkce vrati ukazatel na data teto polozky, jinak vrati NULL
{
  if(T->root == NULL)
    return NULL;
  
  return nodeSearch(T->root, key);
}

tTreeItem *nodeSearch(tTreeItem *node, char *key)
{

  if(node != NULL)
  { 
    if(!strcmp(key, (node->key))) //kdyz jsou stejne klice, nalezli jsme prvek
      return node;
    if(strcmp(key, (node->key)) < 0) 
      return nodeSearch(node->LTreeItem, key);
    else
      return nodeSearch(node->RTreeItem, key);
  }
  else 
    return NULL; 

}


void treeFree(tSymbolTree *T)
// funkce dealokuje strom symbolu
{
  if (T == NULL)
    return;

  nodeFree(T->root);
}

void nodeFree(tTreeItem *node)  
// Dealokuje uzel a jeho poduzly
{
  if(node == NULL)
    return;
  if(node->data.varType == TYPE_STRING){
    stringFree(&node->data.varValue.string_var);
  }


  nodeFree(node->LTreeItem);
  nodeFree(node->RTreeItem);
  free(node->key);
  free(node);
}

void printTree(tSymbolTree *T)
{
  if (T == NULL)
  {
    return;
  }
  printNode(T->root);

}

void dataToStruct(tTreeItemPtr *T, int varType, char *data)
{
  switch(varType){
    case TYPE_STRING:
      (*T)->data.varValue.string_var.allocated = strlen(data) + 1;
      stringCreateWithLenght(&(*T)->data.varValue.string_var);
      (*T)->data.varValue.string_var.length = strlen(data);
      memcpy((*T)->data.varValue.string_var.data,data,strlen(data)+1);
      break;

    case TYPE_DOUBLE:
      (*T)->data.varValue.double_var = atof(data);
      break;

    case TYPE_FUNCTION:
    case TYPE_INT:
      (*T)->data.varValue.int_var = atoi(data);
      break;
  
    case TYPE_BOOL:
      (*T)->data.varValue.bool_var = data[0] == '1' ? true : false; //prozatim
      break;

    case TYPE_NULL:
    case TYPE_VAR:
      break;
  }
}

void printNode(tTreeItem *node) 
{
  if(node == NULL)
  {
    return;
  }
  printf("Key: %s\n", node->key);
  char tmp[5];
  strcpy(tmp,node->data.isTemp ? "ano" : "ne");
  char var[5];
  strcpy(var,node->data.isVar? "ano" : "ne");
  
  switch(node->data.varType)
  {
    case TYPE_INT:
      printf("|--typ: int\ttmp:%s\tvar:%s\thodnota %d\n\n", tmp, var, node->data.varValue.int_var);
      break;
    case TYPE_DOUBLE:
      printf("|--typ: dbl\ttmp:%s\tvar:%s\thodnota: %f\n\n", tmp, var,  node->data.varValue.double_var);
      break;
    case TYPE_STRING:
      printf("|--typ: str\ttmp:%s\tvar:%s\thodnota: %s delka: %d\n\n", tmp, var, node->data.varValue.string_var.data,
      node->data.varValue.string_var.length);
      break;
    case TYPE_VAR:
      printf("|--typ: var\ttmp:%s\tvar:%s\t\n\n", tmp, var);
      break;
    case TYPE_BOOL:
      printf("|--typ: bool\ttmp:%s\tvar:%s\thodnota:%s\n\n", tmp, var,  (node->data.varValue.bool_var == true) ? "true" : "false");
      break;
    case TYPE_NULL:
      printf("|--typ: null\ttmp:%s\tvar:%s\t\n\n", tmp, var);
      break;
    case TYPE_FUNCTION:
      printf("|--typ: fce\ttmp:%s\tvar:%s\targumentu: %d\n\n", tmp, var, node->data.varValue.int_var);
  }
  printNode(node->LTreeItem);
  printNode(node->RTreeItem);
}

int treeCopy(tSymbolTree *Orig, tSymbolTree *Copy)
{
  if(Orig != NULL)
  {
      if(nodeCopy(&(Orig->root), &(Copy->root)))
        return 1;
  }
  
  return 0;
}

int nodeCopy(tTreeItemPtr *Orig, tTreeItemPtr *Copy)
{

  if(*Orig != NULL)
  {
    if ((*Copy = malloc(sizeof(struct treeItem))) == NULL)
      return 1;

    
    if(((*Copy)->key = malloc(sizeof(char)*(strlen((*Orig)->key)+1))) == NULL)
    {
      free(*Copy);
      return 1;
    }
    
    strcpy((*Copy)->key, (*Orig)->key);
    (*Copy)->data.isVar = (*Orig)->data.isVar;
    (*Copy)->data.isTemp = (*Orig)->data.isTemp;
    (*Copy)->data.nextArg = (*Orig)->data.nextArg;
    (*Copy)->RTreeItem = NULL;
    (*Copy)->LTreeItem = NULL;
    
    if(copyData(Orig, Copy))
      return 1;

    if(nodeCopy(&(*Orig)->LTreeItem, &(*Copy)->LTreeItem))
      return 1;
    if(nodeCopy(&(*Orig)->RTreeItem, &(*Copy)->RTreeItem))
      return 1;
  }
  else
    (*Copy) = NULL;
  
  return 0;
}

int copyData(tTreeItemPtr *Orig, tTreeItemPtr *Copy)
{
  (*Copy)->data.varType = (*Orig)->data.varType;
  switch((*Copy)->data.varType){
    case TYPE_STRING:
      if(stringCreate(&((*Copy)->data.varValue.string_var)))
        return 1;
      if(stringCopy(&((*Orig)->data.varValue.string_var),&((*Copy)->data.varValue.string_var)))
        return 1;
      break;

    case TYPE_DOUBLE:
      (*Copy)->data.varValue.double_var = (*Orig)->data.varValue.double_var;
      break;

    case TYPE_FUNCTION:
    case TYPE_INT:
      (*Copy)->data.varValue.int_var = (*Orig)->data.varValue.int_var;
      break;

    case TYPE_BOOL:
      (*Copy)->data.varValue.int_var = (*Orig)->data.varValue.bool_var;
      break;
    
  }
  return 0;
}




//////////////////////// Boyer–Moore ZACATEK ///////////////////////////
//debug
/*
void vypis(TJumpMatchArray *table, int m)
{
  int i;
  for(i = 0; i < m; i++)
    printf("%d. %d\n", i, table->arr[i]);
}*/

int findString(string *str ,string *match, int *result)
{
  int j, k; //j - index do textu  k - index do vzorku
  TJumpCharArray skipTableChar;
  TJumpMatchArray skipTableMatch;
  
  if(stringLen(match) == 0) // delka match je 0, match se nachazi vsude
  {
    *result = 0;
    return 0; //OK
  }
  
  if((skipTableMatch.arr = malloc(sizeof(int) * stringLen(str))) == NULL)
    return -1; // ERROR

  
  // vypocteme match jumpy
  computeMatchJump(match, &skipTableMatch);
  // vypocteme char jumpy
  computeCharJumps(match, &skipTableChar);

  k = j = stringLen(match);
  
  while(j <= stringLen(str) && k > 0)
  {
    if(str->data[j-1] == match->data[k-1])
    {
      j--; k--;
    }
    else
    {
      j = j + max(skipTableChar.arr[(int)str->data[j-1]],skipTableMatch.arr[k-1]);
      k = stringLen(match);
    }

    
  }

  if(k == 0)
    *result = j;
  else
    *result = -1;
    
  free(skipTableMatch.arr);
  return 0; // OK
}

// Vrati mensi cislo
// Pozn. pro prehlednost v extra funkci
int min(int a, int b)
{
  if(a < b)
    return a;
  else 
    return b;
}


// Vrati vetsi cislo
// Pozn. pro prehlednost v extra funkci
int max(int a, int b)
{
  if(a < b)
    return b;
  else 
    return a;
}

// Vypocita o kolik ma algoritmus skocit dopredu
void computeMatchJump(string *str, TJumpMatchArray *skipTableMatch)
{
  int k, q, qq;
  int m = stringLen(str);
  int BackUp[m];

  
  // inicializace
  for(k = 0; k < m; k++)
    skipTableMatch->arr[k] = 2*m - k - 1;
  
  k = m; q = m + 1;
  while(k > 0)
  {
    BackUp[k-1] = q;
    while(q <= m && str->data[k-1] != str->data[q-1])
    {
      skipTableMatch->arr[q-1] = min(skipTableMatch->arr[q-1], m - k);
      q = BackUp[q-1];
    }
    k--;q--;
  }
  
  
  for(k = 1; k <= q; k++)
    skipTableMatch->arr[k-1] = min(skipTableMatch->arr[k-1], m + q - k);
 
  qq = BackUp[q-1];
  while(q < m)
  {
    while(q < qq)
    {
      skipTableMatch->arr[q-1] = min(skipTableMatch->arr[q-1], qq - q + m);
      q++;
    }
    qq = BackUp[qq-1];
  }
}

// Vypocita o kolik ma algoritmus skocit dopredu 
void computeCharJumps(string *str, TJumpCharArray *skipTableChar)
{
  for(int i = 0; i < ALPHABET; i++)
    skipTableChar->arr[i] = stringLen(str);

  for(int k = 0; k < stringLen(str); k++)
    skipTableChar->arr[(int)str->data[k]] = stringLen(str) - k - 1;
}


///////////////////////// Boyer–Moore KONEC ////////////////////////////



////////////////////////// HeapSort ZACATEK ////////////////////////////

void swap (char * array, int a, int b) // vymena dvou prvku v poli
{
  int c; // pomocna promenna
  c = array[a];
  array[a] = array[b];
  array[b] = c;
}

void buildHeap (char * array, int last) // vytvoreni haldy
{
  for (int i = (last-1)/2; i >= 0; i--)
  {
    maxHeapify(array,i,last);
  }
}

void maxHeapify (char * array, int parent, int last) // obnoveni haldy
{
  int child = 2 * parent + 1;
  while ( child <= last )
  {
    if ( (child + 1 <= last) && (array[child+1] > array[child]) )
      child++;

    if ( array[child] > array[parent] )
      swap(array,child,parent);
      parent = child;
      child = 2 * parent + 1;
  }
}

void heapSort (char * array, int last)
{
  buildHeap(array,last);

  while ( last > 0 )
  {
    swap(array,0,last);
    last--;
    floydHeapify(array,last);
  }
}

void floydHeapify (char * array, int last)
{
  int parent = 0, child = 1;

  while ( child <= last )
  {
    if ( (child + 1 <= last) && (array[child] < array[child + 1]) )
      child++;

    swap(array,parent,child);
    parent = child;
    child = 2 * parent + 1;
  }

  while ( (parent > 0) && (array[parent] > array[(parent - 1) / 2]) )
  {
    swap(array,parent,(parent - 1) / 2);
    parent = (parent - 1) / 2;
  }
}
////////////////////////// HeapSort KONEC ////////////////////////////
