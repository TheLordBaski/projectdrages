/*
 * Autory: Jakub Gabčo(teamleader)
 *		   Zdeněk Pečínka
 *		   Adam Jež
 *		   Ján Dedič
 *		   Marek Schmith
 * 
 * Kricí názov: Drages
 *  
 * Popis programu: Interpret pre jazyk IFJ13, ktorý je "podmnožinou" jazyka php.
 * Súbor: syn-anal.c
 *
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include "terror.h"
#include "tstring.h"
#include "ial.h"
#include "lex_anal.h"
#include "syn-anal.h"
#include "tstack.h"
#include "3actree.h"
#include "interpreter.h"
	
const size_t LENGTH_RULE = 10;
/*
 * Parser - Načíta postupnosť tokenov a prevádza nad nou syntakticku analyzu
 *
 */
const Sstate LLCollumn[33] = {
    StateID, StateInt, StateDouble, StateLP, StateRP, StateComma, StateEnd, StateSemiColon,
    StateVar, StateExp, StateStr, StateLBrace, StateRBrace, StateAssign, StatePHP, StateElse, StateFunction, StateIf,
    StateReturn, StateWhile, StateTrue, StateFalse, StateNull, StateBoolVal, StateDoubleVal, StateIntVal, StateStrVal,
    StateGetStr, StatePutStr, StateStrLen, StateGetSBStr, StateFindStr, StateSortStr
};

const Sstate LLRow[15] = {
	Program, Body, FList, Param, Params, RParam, List, Else, Expr, Operand, Inc, String, RString, PrecExp, Print
};
const Sstate Prec[17] ={
	Const, StateVar, StateMul, StateDiv, StatePlus, StateMinus, StateLess, StateMore, StateLessEQ,
	StateMoreEQ, StateEQ, StateNEQ, StateLP, StateRP, StateDot, StateDol, StateSemiColon
};

char *PrecRulez[12] = {
	")E(","E+E","E-E","E*E","E/E","E.E","E>E","E<E","E=>E","E=<E","E===E","E==!E"
};

const Sstate IncFun[10] = {
	StateBoolVal, StateDoubleVal, StateIntVal, StateStrVal,
    StateGetStr, StatePutStr, StateStrLen, StateGetSBStr, StateFindStr, StateSortStr
};

bool RedefInc(Sstate name);
err SyntaxCheck(Token token);
int findCollumn(Sstate state);
int findRule(int collumn,int row);
int derivate(Token*,tStack*,tRule *rulUse);
int findRow(Sstate state);
int EQ(Token token,tStack *stack);
err precedAnal(Token* token,tStack *LLstack);
int PrecXY(Sstate state);
int findPrio(int x,int y);
err PrecFind(tStack *stack);
void freeRule(tRule* ruluse);
int panicRegenerate(Token* token,tStack* stack);
void insertConst(Token token);
void createTree(int rule);
int ProcParam(Token *token,tStack *stack,char *name);
void createNode(int operation, int tmpResult);
void createJump(bool op, int operation);
void createLabel(char *name);
int term(Token *token,tStack *stack,ASSItemPtr ptr,int operation);
int startJump(TSStack *JumpList,tListInstr *ListIstr);
int endJump(Token *token,TSStack *JumpList,tListInstr *ListIstr);
int giveParam(Token *token,tStack *stack);
ASSItemPtr insertParam(Token *token);

FILE* source;
int line;
TSStack STstack;
TSStack *ArgStack;
tStack LastState;
tSymbolTree *BSTTree = NULL; // novy strom ktory bude pouzivať vo funkcii
tSymbolTree *STTree = NULL; // aktualne pouzivnay SymbolTree
tSymbolTree *FSTTree = NULL; // ulozime tam dotacne strom ak budeme vo funkcii
int tempnum = 0;
int PtArgNum = 0;
tListInstr ListIstr;
TSStack JmpStack;
bool PSRet = false;
Token token;
Sstate state;


/*
 * Funkcia Parser:
 * Funkcia zavolá funkciu getToken a zisti ci je prazdny zdrojovy subor ak nie prebehne syntakticka analyza
 * @par = Vsupný súbor
 */
err parser(FILE *file){
	line = 1;
	source = file;
	token=getToken(source,&line);	
	if(token.state == StateEnd){ //Ak je prázdny súbor nebudem pokračovať dalej v syntaktickej analyze
		fprintf(stderr,"Line 1: Unexpected EOF expecting <?php\n");
    free(token.data);
		return SYNTAKTIC_ERR;
	}
	SyntaxCheck(token);
	return E_OK;
}


void initRule(tRule *rulUse){
	rulUse->length = 0;
	if ((rulUse->data = (int*)malloc(sizeof(int)*LENGTH_RULE)) == NULL){
		AllocERR();
		setErr(INTERNAL_ERR);
	}
	rulUse->data[0] = 0;
	rulUse->allocated = LENGTH_RULE;
}


err SyntaxCheck(Token token){
	if((STTree = malloc(sizeof(tSymbolTree))) == NULL){
		setErr(INTERNAL_ERR);
		AllocERR();
		return E_MALLOC;
	}
	tRule rulUse;
	tStack stack;
  assNodesInit(); // inicializace struktury pro sbirani odpadku pri vytvareni instrukci
	treeInit(STTree);
  BSTTree = STTree;
	SSInit(&LastState);
	SSInit(&stack);;
	SSPush(&stack,Program);
	initRule(&rulUse);
	TSSInit(&STstack);
  TSSInit(&JmpStack);
	int fin = 1;
	while(fin){
		if(findRow(stack.top->state) != -1){	//Je to neterminál
			int ret = derivate(&token,&stack,&rulUse);
			if(ret == -1){
				fin=false;
        free(token.data);
				break;
			}
		}
		while(findCollumn(stack.top->state) != -1){  //Je to terminál
			if(EQ(token,&stack) == 0){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack.top->state,token.state,line);
				if(!panicRegenerate(&token,&stack)){
					fin=false;
				}
        free(token.data);
				break;
			}
			if(stack.top->state == StateLBrace&&token.state==StateLBrace){
				startJump(&JmpStack,&ListIstr);
			}
     		if(stack.top->state == StateWhile&&token.state==StateWhile){
         createLabel(NULL); 	
         TSSPush(&JmpStack, ListIstr.last);
     		}
			if(stack.top->state == StateRBrace&&token.state==StateRBrace){
				free(token.data);
				token = getToken(source,&line);
				endJump(&token,&JmpStack,&ListIstr);
				SSPop(&stack);
				break;
			}
			SSPop(&stack);
			if (token.state==StateEnd){
				fin=0;
        free(token.data);
				break;
			}
			free(token.data);
			token=getToken(source,&line);
			
			if(findRow(stack.top->state) != -1)
				break;

		}
	}
	//printf("----------TISKNU INSTRUKCE---------\n");
	//printList(&ListIstr);
	//printf("--------KONEC TISKU INSTRUKCE-------\n\n");

  //Uvolnim nepotrebne ukazatel pro vytvoreni instrukci
	//printf("------------Interpretujem-----------\n");
  	assNodesFree();

	if(ErrRet()==E_OK){
		setErr(interpret(&ListIstr,STTree));
	}

	//printf("\n------------Interpretujem-----------\n\n");
  freeSTstack(&JmpStack);
  SSFree(&LastState);
	SSFree(&stack);
	freeRule(&rulUse);
	//printf("----------TISKNU TREE---------\n");
	//printTree(STTree);
	//printf("----------KONEC TREE---------\n");
	treeFree(BSTTree);
	free(BSTTree);
  //free(FSTTree);
  listFree(&ListIstr);
	freeSTstack(&STstack);
	return E_OK;
}

void freeRule(tRule* ruluse){
	free(ruluse->data);
}

int findCollumn(Sstate state){
	for(int j = 0; j < 33; j++){
		if(!strcmp(States[state],States[LLCollumn[j]])){
			return j;
		}
	}
	return -1;
}



int findRow(Sstate state){
	for(int i = 0; i < 15; i++){
		if(state == LLRow[i])
			return i;
	}
	return -1;
}


int findRule(int collumn,int row){
	return LL_Table[row][collumn];
}

void addRule(tRule* rulUse,int rule){
	if(rulUse->length == rulUse->allocated){
		if((rulUse->data = (int*)realloc(rulUse->data,sizeof(int)*(rulUse->allocated+LENGTH_RULE))) == NULL){
			AllocERR();
			setErr(INTERNAL_ERR);
			return;
		}
		rulUse->allocated += LENGTH_RULE;
	}
	rulUse->data[rulUse->length] = rule;
	rulUse->length+=1;
}


int EQ(Token token,tStack *stack){
	return token.state==stack->top->state?1:0;
}

int derivate(Token *token,tStack *stack,tRule *rulUse){
	int rule,x,y;
	Sstate state;
	SSTop(stack,&state);
	if(stack->top->state == Program && token->state != StatePHP){
    
    
		UnexpErr(StatePHP,token->state,line);		
		setErr(SYNTAKTIC_ERR);
    free(token->data);
    *token = getToken(source,&line);
		SSPop(stack);
		SSPush(stack,StateEnd);
		SSPush(stack,Body);
		return 1;
	}
	else{
		if(((x = findCollumn(token->state)) == -1 || (y = findRow(state))) == -1){
			if(!panicRegenerate(token,stack))
				return -1;
			return 1;
		}
	}
	y = findRow(state);
	rule = findRule(x,y);
	//test

	ASSItemPtr ptr = NULL;
	switch(rule){
		case 0:{
			UnexpErr(stack->top->state,token->state,line);
			setErr(SYNTAKTIC_ERR);
			if(panicRegenerate(token,stack))
				return 1;

			return -1;
		}
		case 1:{ 			//Program -> StatePHP Body StateEnd
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateEnd);
			SSPush(stack,Body);
			SSPush(stack,StatePHP);
			return 1;
		}
		case 2:{   			//Body -> Eps
			addRule(rulUse,rule);
			SSPop(stack);
			return 1;
		}
		case 3:{			//Body -> StateFunction StateID StateLP Param StateRP StateLBrace FList StateRBrace Body
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,Body);
			SSPush(stack,StateRBrace);
			SSPush(stack,FList);
			SSPush(stack,StateLBrace);
			SSPush(stack,StateRP);
			SSPush(stack,Param);
			SSPush(stack,StateLP);
			SSPush(stack,StateID);

			SSPush(&LastState,StateFunction);
      free(token->data); //Uvolnime data tokenu
			//Vkladanie 
			if((FSTTree = malloc(sizeof(tSymbolTree))) == NULL){
				setErr(INTERNAL_ERR);
				AllocERR();
				return E_MALLOC;
			}
      treeInit(FSTTree);
      
      // Zamenime ukazatele
			BSTTree = STTree; // Uložíme si ukazatel na root stromu celého programu
			STTree = FSTTree;
      
			createJump(false, TO_JMP);					      // vytvorím jump 
			TSSPush(&JmpStack, ListIstr.last);
			
			*token = getToken(source,&line);
			if(RedefInc(token->state) == true){
				fprintf(stderr,"Line %d: Redefination of internal function.\n",line);
				setErr(FUNCTION_ERR);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			if(EQ(*token,stack)){
				char *nameS,*name = token->data;
				nameS = malloc(sizeof(char)*(strlen(token->data)+2));
				strcpy(nameS,"$");
				strcat(nameS,token->data);
				if(treeSearch(BSTTree,nameS) != NULL){
					fprintf(stderr,"Line %d: Redefination of function %s().\n",line,name);
					setErr(FUNCTION_ERR);
					free(nameS);
					if(!panicRegenerate(token,stack)){
						return -1;
					}
					return 1;
				}

				free(nameS);
				*token = getToken(source,&line);
				int NumParam = ProcParam(token,stack,name);
				if(NumParam == -1){
					if(!panicRegenerate(token,stack)){
						return -1;
					}
					return 1;
				}

				free(name);
			}
			else{
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			return 1;
		}
		case 4:{			//Body -> List Body
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,Body);
			SSPush(stack,List);
			return 1;
		}
		case 5:{ 			//FList -> Eps
			addRule(rulUse,rule);
			SSPop(stack);
			return 1;
		}
		case 6:{ 			//FList -> List FList
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,FList);
			SSPush(stack,List);
			return 1;
		}
		case 7:{ 			//Param -> Eps
			addRule(rulUse,rule);
			SSPop(stack);
			return 1;
		}
		case 8:{ 			//Param ->Params
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,Params);
			return 1;
		}
		case 9:{ 			//Params -> Operand RParam
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,RParam);
			SSPush(stack,Operand);
			return 1;
		}
		case 10:{ 			//RParam -> Eps
			addRule(rulUse,rule);
			SSPop(stack);
			return 1;
		}
		case 11:{ 			//RParam -> StateComma Params
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,Params);
			SSPush(stack,StateComma);
			return 1;
		}
		case 12:{ 			//List -> StateID StateLP Param StateRP StateSemiColon FList 
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,FList);
			SSPush(stack,StateSemiColon);
			SSPush(stack,StateRP);
			SSPush(stack,Param);
			SSPush(stack,StateLP);
      char *name = malloc(sizeof(char)*(strlen(token->data)+2));
      strcpy(name,"$");
      strcat(name,token->data);
			*token = getToken(source,&line);
			if(EQ(*token,stack) == 0){ //ak tam je niečo ine ako (
        free(name);
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			else{
				SSPop(stack); //Popnem (
				free(token->data);
				*token = getToken(source,&line);
				if(token->state != StateRP){
				
					if((giveParam(token,stack)) ==-1)
						return -1;
				}
        else
          ArgStack = NULL;
				//a Vytvoríme jump
				TA3ac *ins;
				if((ins = malloc(sizeof(TA3ac)))==NULL){
					AllocERR();
					setErr(INTERNAL_ERR);
					return -1;
				}
				if((ins->label = malloc(sizeof(struct sLabel)))==NULL){
					AllocERR();
					setErr(INTERNAL_ERR);
					return -1;
				}
				ins->result = NULL;
				ins->op1 = NULL;
				ins->jump = ArgStack;	
				ins->label->name = name;
				ins->op2 = NULL;
				ins->operation = TO_FCE;
				listInsertLast(&ListIstr, ins);
			}
			return 1;
		}
		case 13:{ 			//List -> StateIf StateLP PrecExp StateRP StateLBrace List StateRBrace Else FList
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,FList);
			SSPush(stack,Else);
			SSPush(stack,StateRBrace);
			SSPush(stack,FList);
			SSPush(stack,StateLBrace);
			SSPush(stack,StateRP);
			SSPush(stack,PrecExp);
			SSPush(stack,StateLP);
			SSPush(stack,StateIf);
			SSPush(&LastState,StateIf);
			return 1;
		}
		case 14:{ 			//List -> StateWhile StateLP PrecExp StateRP StateLBrace List StateRBrace FList
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,FList);
			SSPush(stack,StateRBrace);
			SSPush(stack,FList);
			SSPush(stack,StateLBrace);
			SSPush(stack,StateRP);
			SSPush(stack,PrecExp);
			SSPush(stack,StateLP);
			SSPush(stack,StateWhile);
			SSPush(&LastState,StateWhile);
			return 1;
		}
		case 15:{ 			//List -> StateVar StateAssign Expr StateSemiColon FList
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,FList);
			SSPush(stack,StateSemiColon);
			SSPush(stack,Expr);
			SSPush(stack,StateAssign);
			SSPush(stack,StateVar);
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_VAR,token->data));
			TSSPush(&STstack, ptr);
			return 1;
		}
		case 16:{ 			//List -> StateReturn PrecExp StateSemiColon
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateSemiColon);
			SSPush(stack,PrecExp);
			free(token->data);
			*token = getToken(source,&line);
			
		    //Vytvorime docasnou promennou do ktere ulozime vysledek return vyrazu
		    ass_nodeInsert(&ptr, 0, createTmpVar(tempnum++,STTree)); 
		    TSSPush(&STstack,ptr);
      
			if(findRule(findCollumn(token->state),findRow(stack->top->state))== 0){
				UnexpErr(StateVar,token->state,line);
				setErr(SYNTAKTIC_ERR);
				return -1;
			}
			SSPop(stack);
			if(precedAnal(token,stack) == SYNTAKTIC_ERR){//Zavola sa funkcia ktora spracuje precedencnou tabulkou výrazy
				
				return -1;
			}

      		ass_nodeInsert(&ptr, 0, ListIstr.last->ins->result); 
     		TSSPush(&STstack, ptr);  // result-> do resultu posledni intrukce
			TSSPush(&STstack, NULL); //2.operand
      		TSSPush(&STstack, NULL); //1.operand

			createNode(TO_RET,0);
			//free(token->data);
			return 1;
		}
		case 17:{ 			//List -> Inc StateSemiColon FList
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,FList);
			SSPush(stack,StateSemiColon);
			SSPush(stack,Inc);
			return 1;
		}
		case 18:{ 			//Else -> Eps
			addRule(rulUse,rule);
			SSPop(stack);
			return 1;
		}
		case 19:{ 			//Else -> StateElse StateLBrace List StateRBrace
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRBrace);
			SSPush(stack,FList);
			SSPush(stack,StateLBrace);
			SSPush(stack,StateElse);
			SSPush(&LastState,StateElse);
			return 1;
		}
		case 21:{ 			//Expr -> PrecExp // naše
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,PrecExp);
			return 1;
		}
		case 22:{ 			//Expr -> StateID StateLP Param StateRP 
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,Param);
			SSPush(stack,StateLP);
			char *name = malloc(sizeof(char)*(strlen(token->data)+2));
      strcpy(name,"$");
      strcat(name,token->data);
			free(token->data);//uložíme si meno funckie
			*token = getToken(source,&line);
			if(EQ(*token,stack) == 0){ //ak tam je niečo ine ako (
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;

			}
			else{
				SSPop(stack); //Popnem (
				free(token->data);
				*token = getToken(source,&line);
				if(token->state != StateRP){	
					if((giveParam(token,stack)) == -1){
						return -1;
					}
				}
        else
        {
          ArgStack = NULL;
        }
				//a Vytvoríme jump
				TA3ac *ins;
				if((ins = malloc(sizeof(TA3ac)))==NULL){
					AllocERR();
					setErr(INTERNAL_ERR);
					return -1;
				}
				if((ins->label = malloc(sizeof(struct sLabel)))==NULL){
					AllocERR();
					setErr(INTERNAL_ERR);
					return -1;
				}

			
				if(STstack.top != NULL){
        			ptr = (ASSItemPtr)TSSTop(&STstack);
					if(ptr->operand != NULL){
						if(ptr->operand->data.varType == TYPE_VAR){
							ins->result = ptr->operand;
              				TSSPop(&STstack);
						}
					}
				}
				else{
					ins->result = NULL;
				}
        ins->op1 = NULL;
				ins->jump = ArgStack;
				ins->label->name = name;
				ins->op2 = NULL;
				ins->operation = TO_FCE;
        
				listInsertLast(&ListIstr, ins);
			}
			return 1;
		}
		case 23:{ 			//Expr -> Inc
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,Inc);
			PSRet = true;
			return 1;
		}
		case 24:{ 			//Inc -> StateBoolVal StateLP PrecExp StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,PrecExp);
			SSPush(stack,StateLP);
			return term(token,stack,ptr,TO_BVAL);
		}
		case 25:{ 			//Inc -> StateDoubleVal StateLP PrecExp StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,PrecExp);
			SSPush(stack,StateLP);
			return term(token,stack,ptr,TO_DVAL);
		}
		case 26:{ 			//Inc -> StateIntVal StateLP PrecExp StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,PrecExp);
      SSPush(stack,StateLP);
			return term(token,stack,ptr,TO_IVAL);
		}
		case 27:{ 			//Inc -> StateStrVal StateLP PrecExp StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,PrecExp);
			SSPush(stack,StateLP);
			return term(token,stack,ptr,TO_SVAL);
		}
		case 28:{ 			//Inc -> StateGetStr StateLP StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,StateLP);
			SSPush(stack,StateGetStr);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateLP){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			else{
				SSPop(stack);
				free(token->data);
				*token = getToken(source,&line);
			}
			if(token->state != StateRP){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			else{
				SSPop(stack);
				free(token->data);
				*token = getToken(source,&line);
			}
			TSSPush(&STstack, NULL);
			TSSPush(&STstack, NULL);
			createNode(TO_GSTR,0);
			SSPop(stack);
			return 1;
		}
		case 29:{ 			//Inc -> StatePutStr StateLP String StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,String);
			SSPush(stack,StateLP);
			SSPush(stack,StatePutStr);
			return 1;
		}
		case 30:{ 			//Inc -> StateStrLen StateLP StateStr StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,Print);
			SSPush(stack,StateLP);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateLP){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
      SSPop(stack); // Popuju levou zavorku
      free(token->data);
      // Chci dalsi token -> nejakou promennou nebo konstantu
			*token = getToken(source,&line);
			if((ptr = insertParam(token))==NULL)
				return -1;
      SSPop(stack);//Dostal jsem promennou -> popuju
      free(token->data);
      
			TSSPush(&STstack, ptr); // prvni operand
      TSSPush(&STstack, NULL); // druhy operand  neexistuje
			createNode(TO_SLEN, 0);
			
      //Zazadam o dalsi token
			*token = getToken(source,&line);
			return 1;
		}
		case 31:{ 			//Inc -> StateGetSBStr StateLP StateStr StateInt StateInt StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateLP){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			free(token->data);
			*token = getToken(source,&line);
			if((ptr = insertParam(token))==NULL)
				return -1;
			TSSPush(&STstack,ptr);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateComma){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			free(token->data);
			*token = getToken(source,&line);
			if((ptr = insertParam(token))==NULL)
				return -1;
			TSSPush(&STstack,ptr);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateComma){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			free(token->data);
			*token = getToken(source,&line);
			if((ptr = insertParam(token))==NULL)
				return -1;
			TSSPush(&STstack,ptr);
			free(token->data);
			*token = getToken(source,&line);
			TA3ac *ins;
			if((ins = malloc(sizeof(TA3ac)))==NULL){
				AllocERR();
				setErr(INTERNAL_ERR);
				return -1;
			}
      ptr = STstack.top->ptr;
			ins->jump = ptr->operand;
			TSSPop(&STstack);
			
			ptr = STstack.top->ptr;
			ins->op2 = ptr->operand;	
      TSSPop(&STstack);
      
			ptr = STstack.top->ptr;
			ins->op1 = ptr->operand;
			TSSPop(&STstack);
			
			ptr = STstack.top->ptr;
			if(PSRet == true){
				TSSPop(&STstack);
				ins->result = ptr->operand;
			}
			else{
				ins->result = NULL;
			}
			ins->label = NULL;
			ins->operation = TO_GSSTR;
			listInsertLast(&ListIstr, ins);
			//printStack(stack);
			//printf("Token %s\n",States[token->state]);
			return 1;
		}
		case 32:{ 			//Inc -> StateFindStr StateLP StateStr StateStr StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,Print);
			SSPush(stack,StateComma);
			SSPush(stack,Print);
			SSPush(stack,StateLP);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateLP){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			while(token->state != StateStr && token->state != StateVar){
				if(EQ(*token,stack)){
					SSPop(stack);
					free(token->data);
					*token = getToken(source,&line);
				}
				else{
					setErr(SYNTAKTIC_ERR);
					UnexpErr(stack->top->state,token->state,line);
					if(!panicRegenerate(token,stack)){
						return -1;
					}
					return 1;
				}

			}
			if((ptr = insertParam(token))==NULL)
				return -1;
			SSPop(stack);
			TSSPush(&STstack, ptr); // prvni operand
			free(token->data);
			*token = getToken(source,&line);
			if(token->state == StateComma){
				free(token->data);
				*token = getToken(source,&line);
				SSPop(stack);
			}
			else{
				setErr(SYNTAKTIC_ERR);
				UnexpErr(StateStr,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			if((ptr = insertParam(token))==NULL)
				return -1;
      		TSSPush(&STstack, ptr);
			createNode(TO_FSTR, 0);
			SSPop(stack);
			free(token->data);
			*token = getToken(source,&line);
			return 1;
		}
		case 33:{ 			//Inc -> StateSortStr StateLP StateStr StateRP
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateRP);
			SSPush(stack,Print);
			SSPush(stack,StateLP);
			free(token->data);
			*token = getToken(source,&line);
			if(token->state != StateLP){
				setErr(SYNTAKTIC_ERR);
				UnexpErr(stack->top->state,token->state,line);
				if(!panicRegenerate(token,stack)){
					return -1;
				}
				return 1;
			}
			while(token->state != StateStr && token->state != StateVar){
				if(EQ(*token,stack)){
					SSPop(stack);
					free(token->data);
					*token = getToken(source,&line);
				}
				else{
					setErr(SYNTAKTIC_ERR);
					UnexpErr(stack->top->state,token->state,line);
					if(!panicRegenerate(token,stack)){
						return -1;
					}
					return 1;
				}

			}
			if((ptr = insertParam(token))==NULL)
				return -1;
			TSSPush(&STstack, ptr); // prvni operand
      		TSSPush(&STstack, NULL); // druhy operand je neexistuje
			createNode(TO_SSTR, 0);
			SSPop(stack);
			free(token->data);
			*token = getToken(source,&line);
			return 1;
		}
		case 34:{ 			//String -> StateStr RString
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,RString);
			SSPush(stack,Print);
			if(token->state == StateRP && PtArgNum == 0){ // prázdny put_string
				if(STstack.top != NULL){
          			ptr = (ASSItemPtr)TSSTop(&STstack);
					if(ptr->operand != NULL){
						if(ptr->operand->data.varType == TYPE_VAR&&PSRet == true){
							char ArgNum[4];
							sprintf(ArgNum,"%d",PtArgNum);
							ass_nodeInsert(&ptr, 0, treeInsert(STTree,ArgNum,TYPE_INT,ArgNum));
							TSSPush(&STstack, ptr);
							createNode(TO_ASS, 0);
						}
					}
				}
				SSPop(stack);
				return 1;
			}			 
			if((ptr = insertParam(token))==NULL)
				return -1;
			++PtArgNum;
			TSSPush(&STstack, ptr);
			createNode(TO_PSTR, 0);
			SSPop(stack);
			free(token->data);
			*token = getToken(source,&line);
			return 1;
		}
		case 35:{ 			//RString -> Eps
			addRule(rulUse,rule);
			SSPop(stack);
			if(token->state == StateRP){ // prázdny put_string
				if(STstack.top != NULL){
          ptr = (ASSItemPtr)TSSTop(&STstack);
          if(ptr->operand != NULL){
            if(ptr->operand->data.varType == TYPE_VAR&&PSRet == true){
              char ArgNum[4];
              sprintf(ArgNum,"%d",PtArgNum);
              ass_nodeInsert(&ptr, 0, treeInsert(STTree,ArgNum,TYPE_INT,ArgNum));
              PtArgNum = 0;
              TSSPush(&STstack, ptr);
              createNode(TO_ASS, 0);
            }
          }
				}
				return 1;
			}

			return 1;
		}
		case 36:{ 			//RString -> StateComma StateStr RString
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,RString);

			free(token->data);
			*token = getToken(source,&line);
			if((ptr = insertParam(token))==NULL)
				return -1;
			TSSPush(&STstack, ptr);
			createNode(TO_PSTR, 0);
			++PtArgNum;
			free(token->data);
			*token = getToken(source,&line);
			if(token->state == StateRP){ // prázdny put_string
				if(STstack.top != NULL){
        			ptr = (ASSItemPtr)TSSTop(&STstack);
					if(ptr->operand != NULL){
						if(ptr->operand->data.varType == TYPE_VAR&&PSRet == true){
							char ArgNum[4];
							sprintf(ArgNum,"%d",PtArgNum);
							ass_nodeInsert(&ptr, 0, treeInsert(STTree,ArgNum,TYPE_INT,ArgNum));
							PtArgNum = 0;
							TSSPush(&STstack, ptr);
							createNode(TO_ASS, 0);
							PSRet = false;
						}
					}
				}
				SSPop(stack);
				return 1;
			}
			return 1;
		}
		case 37:{ 			//Operand -> StateInt 
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateInt);
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_INT,token->data));
			TSSPush(&STstack, ptr);
			return 1;
		}
		case 38:{ 			//Operand -> StateDouble
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateDouble);
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_DOUBLE,token->data));
			TSSPush(&STstack, ptr);
			return 1;
		}
		case 39:{ 			//Operand -> StateExp
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateExp);
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_DOUBLE,token->data));
			TSSPush(&STstack, ptr);
			return 1;
		}
		case 40:{ 			//Operand -> StateStr
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateStr);
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_STRING,token->data));
			TSSPush(&STstack, ptr);
			return 1;
		}
		case 41:{ 			//Operand -> StateVar
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateVar);
			return 1;
		}
		case 42:{ 			//Operand -> StateNull
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateNull);
			return 1;
		}
		case 43:{ 			//Operand -> StateTrue
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateFalse);
			return 1;
		}
		case 44:{ 			//Operand -> StateFalse
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateTrue);
			return 1;
		}
		case 45:{ 			//PrecAnal
			addRule(rulUse,rule);
			SSPop(stack);
			if(precedAnal(token,stack) == SYNTAKTIC_ERR){//Zavola sa funkcia ktora spracuje precedencnou tabulkou výrazy
				
				return -1;
			}
			return 1;
		}
		case 46:{			//Print -> StateStr
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateStr);
			return 1;
		}
		case 47:{			//Print -> StateVar
			addRule(rulUse,rule);
			SSPop(stack);
			SSPush(stack,StateVar);
			return 1;
		}
	}


	return 1;
}

err precedAnal(Token* token,tStack *LLstack){
	tStack stack;
	tSymbolTree tree;
	treeInit(&tree);
	SSInit(&stack);
	SSPush(&stack,StateDol);
	Sstate state;
	state = stack.top->state;
	int x;
	int y;
	int prio;
	do{
		x = PrecXY(token->state);
		y = PrecXY(state);
		if(x == -1){
			setErr(SYNTAKTIC_ERR);
			fprintf(stderr,"Line %d: Unexpected token %s.\n",line,UStates[token->state]);
			return SYNTAKTIC_ERR;
		}
		if(y == -1){
			setErr(SYNTAKTIC_ERR);
			fprintf(stderr,"Line %d: Unexpected token %s.\n",line,UStates[token->state]);
			return SYNTAKTIC_ERR;
		}
		prio = findPrio(x,y);
		if(x == -1){

		}
		if(y == -1){
			
		}
		if(token->state == StateWhile||token->state == StateIf||token->state==StateReturn||token->state==StateRBrace||token->state==StateFunction){
			
			prio = 2;
		}
		if(stack.top->state==StateInt||stack.top->state==StateStr||stack.top->state==StateDouble||
			stack.top->state==StateExp||stack.top->state==StateTrue||stack.top->state==StateFalse||
			stack.top->state==StateNull||stack.top->state==E){
			if(token->state==StateInt||token->state==StateStr||token->state==StateDouble||token->state==StateExp
				||token->state==StateTrue||token->state==StateFalse||token->state==StateNull||token->state==E){
				setErr(SYNTAKTIC_ERR);
				fprintf(stderr,"Line %d: Unexpected token %s.\n",line,UStates[token->state]);
				return SYNTAKTIC_ERR;
			}
		}
		if(stack.top->state==StatePlus||stack.top->state==StateMinus||stack.top->state==StateMul||
			stack.top->state==StateDiv||stack.top->state==StateDot||stack.top->state==StateLess||
			stack.top->state==StateLessEQ||stack.top->state==StateMore||stack.top->state==StateMoreEQ||
			stack.top->state==StateEQ||stack.top->state==StateNEQ){
			if(token->state==StatePlus||token->state==StateMinus||token->state==StateMul||
				token->state==StateDiv||token->state==StateDot||token->state==StateLess||token->state==StateMore||
				token->state==StateLessEQ||token->state==StateMoreEQ||token->state==StateEQ||token->state==StateNEQ){
				setErr(SYNTAKTIC_ERR);
				fprintf(stderr,"Line %d: Unexpected token %s.\n",line,UStates[token->state]);
				return SYNTAKTIC_ERR;
			}
		}
		switch(prio){
			case 0:{
				if(LLstack->top->state == StateRP){
					Sstate LLState= SSFindTerm(&stack);
					if(LLState == StateDol ){
						SSFree(&stack);
						return E_OK;
					}
					else{
						prio = 2;
						break;
					}
				}
				setErr(SYNTAKTIC_ERR);
				fprintf(stderr,"Line %d: Unexpected token %s.\n",line,UStates[token->state]);
				return SYNTAKTIC_ERR;
			}
			case 1:{
				SSInsert(&stack);
				SSPush(&stack,token->state);
				insertConst(*token);
				free(token->data);
				*token=getToken(source,&line);				
				state = stack.top->state;
				break;
			}
			case 2:{
				PrecFind(&stack);
				state = SSFindTerm(&stack);
				break;
			}
			case 3:{
				SSPush(&stack,token->state);
				insertConst(*token);
				free(token->data);
				*token=getToken(source,&line);
				state = SSFindTerm(&stack);
				break;				
			}
		}
	}while((stack.top->state != StateDol)||(isTerminal(token->state)));
	createTree(0);
	SSFree(&stack);
	return E_OK;
}
int PrecXY(Sstate state){
	if((state == StateInt)||(state == StateDouble)||(state == StateExp)||(state == StateStr)||(state == StateNull)||(state == StateTrue)||(state == StateFalse))
		state = Const;
	for(int i = 0; i < 17; i++){
		if(state == Prec[i])
			return i;
	}
	return -1;
}
int findPrio(int x,int y){
	return Prec_Table[y][x];
}

err PrecFind(tStack *stack){
	string str;
	stringCreate(&str);
	while(stack->top->state != StateLCom){
		if(stack->top->state == StateVar){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateInt){
			SSPop(stack);
			stack->top->state = E;	
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateDouble){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateExp){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateStr){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateTrue){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateFalse){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateNull){
			SSPop(stack);
			stack->top->state = E;
			free(str.data);
			return E_OK;
		}
		if(stack->top->state == StateRP){
			stringAdd(&str,')');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == E){
			stringAdd(&str,'E');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateLP){
			stringAdd(&str,'(');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StatePlus){
			stringAdd(&str,'+');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateMinus){
			stringAdd(&str,'-');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateDiv){
			stringAdd(&str,'/');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateMul){
			stringAdd(&str,'*');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateDot){
			stringAdd(&str,'.');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateLess){
			stringAdd(&str,'<');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateMore){
			stringAdd(&str,'>');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateLessEQ){
			stringAdd(&str,'=');
			stringAdd(&str,'<');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateMoreEQ){
			stringAdd(&str,'=');
			stringAdd(&str,'>');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateEQ){
			stringAdd(&str,'=');
			stringAdd(&str,'=');
			stringAdd(&str,'=');
			SSPop(stack);
			continue;
		}
		if(stack->top->state == StateNEQ){
			stringAdd(&str,'=');
			stringAdd(&str,'=');
			stringAdd(&str,'!');
			SSPop(stack);
			continue;
		}

		free(str.data);
		return SYNTAKTIC_ERR;
	}
	int rule = 0;
	for(int i = 0; i< 12;i++){
		if (stringConstCmp(&str,PrecRulez[i]) == 0){
			rule = i;
			break;
		}
	}
	switch(rule+1){
		case 0:
			break;

		case 1:{
			stack->top->state = E;
			break;
		}
		default:{
			createTree(rule+3);
			SSPop(stack);
			SSPush(stack,E);
			break;
		}

	}
	free(str.data);
	return E_OK;
}


int panicRegenerate(Token* token,tStack* stack){
	if(stack->top->state == StateSemiColon){
		fprintf(stderr,"Line %d: Expecting ; before %s.\n",line,UStates[token->state]);
		setErr(SYNTAKTIC_ERR);
		SSPop(stack);
		return 1;
	}
	else{
		while((stack->top->state != StateSemiColon)&&(stack->top->state != StateEnd)){
			SSPop(stack);
		}
		if(stack->top->state == StateEnd)
			return 0;
		while(true){
			free(token->data);
			*token = getToken(source,&line);
			if(token->state == StateEnd)
				return 0;
			if(token->state == StateSemiColon)
				break;
		}
		return 1;
	}
	return 0;
}
void insertConst(Token token){
  ASSItemPtr ptr = NULL;
	switch(token.state){
		case StateVar:{
      // Neni potreba, resi interpret, vyhodi chybu, ikdyz se k tomu program nedostane
			/*if(!treeSearch(STTree,token.data)){
				fprintf(stderr,"Line %d: Undefined variable %s.\n",line,token.data);
				setErr(UNDEC_VAR_ERR);
			}*/
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token.data,TYPE_VAR,token.data));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateInt:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,token.data,TYPE_INT,token.data));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateDouble:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,token.data,TYPE_DOUBLE,token.data));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateExp:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,token.data,TYPE_DOUBLE,token.data));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateNull:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$n",TYPE_NULL,token.data));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateTrue:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$t",TYPE_BOOL,"1"));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateFalse:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$f",TYPE_BOOL,"0"));
			TSSPush(&STstack, ptr);
			break;
		}
		case StateStr:{
		  ass_nodeInsert(&ptr, 0, treeInsert(STTree,token.data,TYPE_STRING,token.data));
			TSSPush(&STstack, ptr);
			break;
		}
		default:
			break;
	}
}

void createLabel(char *name){
	TA3ac *ins;
	if((ins = malloc(sizeof(TA3ac)))==NULL){
		AllocERR();
		setErr(INTERNAL_ERR);
		return;
	}
	ins->label = NULL;
	if(name != NULL){
		if((ins->label = malloc(sizeof(struct sLabel)))==NULL){
			AllocERR();
			setErr(INTERNAL_ERR);
			return;
		}
		ins->label->name = name;
	}
	ins->result = NULL;
	ins->op1 = NULL;
	ins->op2 = NULL;
	ins->operation = TO_LABEL; 
	listInsertLast(&ListIstr, ins);
}

void createJump(bool op, int operation){
	TA3ac *ins; ASSItemPtr ptr;
	if((ins = malloc(sizeof(TA3ac)))==NULL){
		AllocERR();
		setErr(INTERNAL_ERR);
		return;
	}
	ins->result = NULL;
	ins->op1 = NULL;
	ins->op2 = NULL;	
	ins->label = NULL;
	ins->jump = NULL;
	ins->operation = operation;
	if(op == true){
		ptr = (ASSItemPtr)TSSTop(&STstack);
    ins->op1 = ptr->operand;
	}
	listInsertLast(&ListIstr, ins);
}

void createNode(int operation, int tmpResult){
	ASSItemPtr ptr = NULL;
  
	// Alokujeme pro 3 adresny kod
	TA3ac *ins;
	if((ins = malloc(sizeof(TA3ac)))==NULL){
		AllocERR();
		setErr(INTERNAL_ERR);
		return;
	}
  
	// Inicializace
	ins->result = NULL;
	ins->op1 = NULL;
	ins->op2 = NULL;
	ins->label = NULL;
	//ulozime operaci
	ins->operation = operation;
	/*if(operation == TO_RET){
  		TSSPop(&STstack);
 		TSSPop(&STstack);
		listInsertLast(&ListIstr, ins);
		return;
	}*/
	  
	// Resime druhy operand
	ptr = STstack.top->ptr;
  TSSPop(&STstack);
  if(ptr != NULL)
  {
    ins->op2 = ptr->operand;
    if(operation == TO_PSTR){
      listInsertLast(&ListIstr, ins);
      return;
    }	
  }
	  
	// Resime prvni operand
	ptr = STstack.top->ptr;
  TSSPop(&STstack);
  if(ptr != NULL)
  {
    ins->op1 = ptr->operand;
  }
	  
	// Resime kam dat vysledek operace
	if(operation == TO_ASS)	{
		ins->result = ins->op1;
		ins->op1 = ins->op2;
		ins->op2 = NULL;
	}
	else if(tmpResult){
		ins->result = createTmpVar(tempnum++,STTree);
		ass_nodeInsert(&ptr, 0, ins->result); 
    TSSPush(&STstack,ptr);
	}
  else{
    ptr = STstack.top->ptr;
    TSSPop(&STstack);
    ins->result = ptr->operand;
  }
  

	listInsertLast(&ListIstr, ins);
		
	
}

void createTree(int rule){
	switch(rule){
		case 0:{
			createNode(TO_ASS, 1);
			break;
		}
		case 4:{
			createNode(TO_ADD, 1);
			break;
		}
		case 5:{
			createNode(TO_SUB, 1);
			break;
		}
		case 6:{
			createNode(TO_MUL, 1);
			break;
		}
		case 7:{
			createNode(TO_DIV, 1);
			break;
		}
		case 8:{
			createNode(TO_CAT, 1);
			break;
		}
		case 9:{
			createNode(TO_GREAT, 1);
			break;
		}
		case 10:{
			createNode(TO_LESS, 1);
			break;
		}
		case 11:{
			createNode(TO_GEQ, 1);
			break;
		}
		case 12:{
			createNode(TO_LEQ, 1);
			break;
		}
		case 13:{
			createNode(TO_EQ, 1);
			break;
		}
		case 14:{
			createNode(TO_NQ, 1);
			break;
		}
		default:
			break;
	}
	//printf("Vrch zasobinku: %s\n",STstack.top->ptr->key);
	return;
}


/*Vrati pocet parametrov pri deklaracii funkcii*/
int ProcParam(Token *token,tStack *stack,char *name){
	int NumParam = 0;
	SSPop(stack);
	if(EQ(*token,stack)){ //Lava zavorka
		free(token->data);
		*token = getToken(source,&line);
	}
	else{
		setErr(SYNTAKTIC_ERR);
		UnexpErr(stack->top->state,token->state,line);
		panicRegenerate(token,stack);
		return -1;
	}
	SSPop(stack); //dam prec (
	SSPop(stack); //dam prec Param


	tTreeItemPtr ptr = NULL;

	if(token->state == StateStr||token->state==StateInt||token->state==StateExp||token->state==StateDouble||token->state==StateVar||
		token->state==StateNull||token->state==StateTrue||token->state==StateFalse){ //Spracujem prvy parameter a vlozim do stromu
		if(token->state == StateVar){
			ptr = treeInsert(STTree,token->data,TYPE_VAR,token->data);
		}
		else{
			UnexpErr(StateVar,token->state,line);
			setErr(SYNTAKTIC_ERR);
			return -1;
		}
		NumParam += 1; //zvacsim pocet parametrov na 1
	}
	if(token->state == StateRP){
		char num[5];
		sprintf(num,"%d",NumParam);
		tTreeItemPtr LabName = treeInsert(BSTTree,name,TYPE_FUNCTION,num);
		//ass_nodeInsert(&ptr, 0, LabName); //Vyhladame si meno v tabulke prvkov ktore vlozime do labelu (aby sme nemuseli 2x allokovat pre to)
		
    createLabel(LabName->key);
    LabName->data.nextArg = ListIstr.last;
    ListIstr.last->ins->jump = STTree;
		
		return NumParam;
	}
	tTreeItemPtr first=NULL,last=NULL;
	last = first = STTree->root;

	free(token->data);
	*token = getToken(source,&line);
	while(token->state!= StateRP){
		if(token->state != StateComma){
			UnexpErr(StateComma,token->state,line);
			setErr(SYNTAKTIC_ERR);
			return -1;
		}
		free(token->data);
		*token = getToken(source,&line);
		if(findRule(findCollumn(token->state),findRow(stack->top->state))== 0){
			UnexpErr(StateVar,token->state,line);
			setErr(SYNTAKTIC_ERR);
			return -1;
		}
		if(treeSearch(STTree, token->data) != NULL){
      setErr(SEMANTIC_ERR);
    }
    
    

		if(token->state == StateVar){
			ptr = treeInsert(STTree,token->data,TYPE_VAR,token->data);
		}
		else{
			UnexpErr(StateVar,token->state,line);
			setErr(SYNTAKTIC_ERR);
			return -1;
		}
		last->data.nextArg = ptr;
   		last = ptr;
		NumParam += 1;
		free(token->data);
		*token = getToken(source,&line);
	}
	last->data.nextArg = NULL;
	char num[5];
	sprintf(num,"%d",NumParam);
	tTreeItemPtr LabName = treeInsert(BSTTree,name,TYPE_FUNCTION,num);
	//ass_nodeInsert(&ptr, 0, LabName); //Vyhladame si meno v tabulke prvkov ktore vlozime do labelu (aby sme nemuseli 2x allokovat pre to)
  
  	createLabel(LabName->key);
  	LabName->data.nextArg = ListIstr.last;
  	ListIstr.last->ins->jump = STTree;

	return NumParam;
}

int term(Token *token,tStack *stack,ASSItemPtr ptr,int operation){
	free(token->data);
	*token = getToken(source,&line);
	if(token->state != StateLP){
		setErr(SYNTAKTIC_ERR);
		UnexpErr(stack->top->state,token->state,line);
		if(!panicRegenerate(token,stack)){
			return -1;
		}
		return 1;
	}
	else{
		SSPop(stack);
		free(token->data);
		*token = getToken(source,&line);
	}
	if(token->state == StateStr||token->state==StateInt||token->state==StateExp||token->state==StateDouble||token->state==StateVar||
		token->state==StateNull||token->state==StateTrue||token->state==StateFalse){
		if(token->state == StateVar){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_VAR,token->data));
		}
		if(token->state == StateExp||token-> state == StateDouble){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_DOUBLE,token->data));
		}
		if(token->state == StateTrue){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$t",TYPE_BOOL,"1"));
		}
		if(token->state == StateFalse){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$f",TYPE_BOOL,"0"));
		}
		if(token->state == StateInt){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_INT,token->data));
		}
		if(token->state == StateNull){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$n",TYPE_NULL,token->data));
		}
		if(token->state == StateStr){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_STRING,token->data));
		}
		TSSPush(&STstack, ptr);
		TSSPush(&STstack, NULL);
		createNode(operation,0);
		SSPop(stack);
		free(token->data);
		*token = getToken(source,&line);
	}
	else {
		setErr(SYNTAKTIC_ERR);
		UnexpErr(stack->top->state,token->state,line);
		if(!panicRegenerate(token,stack)){
			return -1;
		}
		return 1;
	}
	return 1;
}


int startJump(TSStack *JumpList,tListInstr *ListIstr){
	Sstate state;
	SSTop(&LastState,&state);
	switch(state){
		case StateIf:
			createJump(true, TO_JMP); // vytvorim jump
       		TSSPush(JumpList, ListIstr->last);
			break;
      
   		case StateWhile:
        createJump(true, TO_JMP); // vytvorim jump
     		TSSPush(JumpList, ListIstr->last);
     		break;
		default:
			break;
	}
	return 0;
}
int endJump(Token *token,TSStack *JumpList,tListInstr *ListIstr){
	Sstate state; tLItem tmp;
	SSTop(&LastState,&state);
	if(token->state == StateElse){
		state = Else;
	}
	switch(state){

		case StateFunction:{ // Toto je else
			tmp = (tLItem)TSSTop(JumpList);				// vrch zásobníku .potrebujeme aby sme vedeli kde mám zapísať label
			TSSPop(JumpList);					        // zoberem vrch zásobníku(už nám tam netreba)

			createJump(false, TO_FFCE);				// vytvorím jump z funkce
			createLabel(NULL); 		            // vytvorím label na ktery budeme preskakovat 
      
      tmp->ins->jump = ListIstr->last; 
      

      //Prohodime zpatky stromy
      FSTTree = NULL;
      STTree = BSTTree;
			break;
		}
		case Else:{ // Toto je else
			tmp = (tLItem)TSSTop(JumpList);		// vrch zásobníku .potrebujeme aby sme vedeli kde mám zapísať label
			TSSPop(JumpList);					        // zoberem vrch zásobníku(už nám tam netreba)
      
			createJump(false, TO_JMP);					      // vytvorím jump 
			TSSPush(JumpList, ListIstr->last);//pushnem poslednu instruciu na zásobník
      
			createLabel(NULL); 		            //vytvorím label
      
			tmp->ins->jump = ListIstr->last;  //vlozim do tmp čo je paritny jump ukazatel na label
      

			break;
		}
		case StateElse:
		case StateIf:
			tmp = (tLItem)TSSTop(JumpList);
      		TSSPop(JumpList);
      
			createLabel(NULL);
			tmp->ins->jump = ListIstr->last;
      
			break;
      
    case StateWhile:
      tmp = (tLItem)TSSTop(JumpList); // Mam tu podminkovy jump
      TSSPop(JumpList);
      
      createJump(false, TO_JMP);
      ListIstr->last->ins->jump = (tLItem)TSSTop(JumpList);
      TSSPop(JumpList);
      
      createLabel(NULL);
      tmp->ins->jump = ListIstr->last;
		default:
			break;
	}
	SSPop(&LastState);
	return 0;
}

int giveParam(Token *token,tStack *stack){

	if(findRule(findCollumn(token->state),findRow(stack->top->state))== 0){
		UnexpErr(StateVar,token->state,line);
		return -1;
	}
	tTreeItemPtr ptr = NULL;
  TSStack stackTmp; TSSInit(&stackTmp); //inicializace docasneho zasobniku
  
  //Allocate memory for stack
  if((ArgStack = malloc(sizeof(TSStack))) == NULL){
    setErr(INTERNAL_ERR);
    AllocERR();
    return -1;
  }
  TSSInit(ArgStack); //init ArgStack
  
 
	if(token->state == StateVar){
		ptr = treeInsert(STTree,token->data,TYPE_VAR,token->data);
	}
	if(token->state == StateExp||token-> state == StateDouble){
		ptr = treeInsert(STTree,token->data,TYPE_DOUBLE,token->data);
	}
	if(token->state == StateTrue){
		ptr = treeInsert(STTree,"$t",TYPE_BOOL,"1");
	}
	if(token->state == StateFalse){
		ptr = treeInsert(STTree,"$f",TYPE_BOOL,"0");
	}
	if(token->state == StateInt){
		ptr = treeInsert(STTree,token->data,TYPE_INT,token->data);
	}
	if(token->state == StateNull){
		ptr = treeInsert(STTree,token->data,TYPE_NULL,token->data);
	}
	if(token->state == StateStr){
			ptr = treeInsert(STTree,token->data,TYPE_STRING,token->data);
	}
  TSSPush(&stackTmp, ptr); //ulozime prvni argument

	free(token->data);
	*token = getToken(source,&line);
	while(token->state!= StateRP){
		if(token->state != StateComma){
			UnexpErr(StateComma,token->state,line);
		}
		free(token->data);
		*token = getToken(source,&line);
		if(findRule(findCollumn(token->state),findRow(stack->top->state))== 0){
			UnexpErr(StateVar,token->state,line);
			return -1;
		}
		ptr = NULL;
    
		if(token->state == StateVar){
			ptr = treeInsert(STTree,token->data,TYPE_VAR,token->data);
		}
		if(token->state == StateExp||token-> state == StateDouble){
			ptr = treeInsert(STTree,token->data,TYPE_DOUBLE,token->data);
		}
		if(token->state == StateTrue){
			ptr = treeInsert(STTree,"$t",TYPE_BOOL,"1");
		}
		if(token->state == StateFalse){
			ptr = treeInsert(STTree,"$f",TYPE_BOOL,"0");
		}
		if(token->state == StateInt){
			ptr = treeInsert(STTree,token->data,TYPE_INT,token->data);
		}
		if(token->state == StateNull){
			ptr = treeInsert(STTree,token->data,TYPE_NULL,token->data);
		}
		if(token->state == StateStr){
			ptr = treeInsert(STTree,token->data,TYPE_STRING,token->data);
		}

		TSSPush(&stackTmp, ptr); //ulozime dalsi argument
		free(token->data);
		*token = getToken(source,&line);
	}

  //Napushujem vsechny argumenty
	while(!TSSEmpty(&stackTmp)){
		TSSPush(ArgStack,TSSTop(&stackTmp));
    TSSPop(&stackTmp);
	}

	SSPop(stack);
	return 1;
}
bool RedefInc(Sstate name){
	for(int i = 0; i < 10;i++){
		if(name == IncFun[i])
			return true;
	}
	return false;
}

ASSItemPtr insertParam(Token *token){
ASSItemPtr ptr = NULL;

	if(token->state == StateStr||token->state==StateInt||token->state==StateExp||token->state==StateDouble||token->state==StateVar||
		token->state==StateNull||token->state==StateTrue||token->state==StateFalse){
		if(token->state == StateVar){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_VAR,token->data));
		}
		if(token->state == StateExp||token-> state == StateDouble){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_DOUBLE,token->data));
		}
		if(token->state == StateTrue){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$t",TYPE_BOOL,"1"));
		}
		if(token->state == StateFalse){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$f",TYPE_BOOL,"0"));
		}
		if(token->state == StateInt){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_INT,token->data));
		}
		if(token->state == StateNull){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,"$n",TYPE_NULL,token->data));
		}
		if(token->state == StateStr){
			ass_nodeInsert(&ptr, 0, treeInsert(STTree,token->data,TYPE_STRING,token->data));
		}
	}
	else {
		setErr(SYNTAKTIC_ERR);
		UnexpErr(StateVar,token->state,line);
		return NULL;
	}
	return ptr;
}
